1. go to folder with manage.py

2. run command: ./manage.py read_file

3. wait until you see the sign 'All position saved!'

4. run: cd myspider

5. run: scrapy crawl rozetka_info
	wait until spider close with results of crawling

6. run: scrapy crawl rozetka_moto
	wait until spider close with results of crawling

7. run: scrapy crawl rozetka
	wait until spider close with results of crawling

8. run: scrapy crawl bfshina
	wait until spider close with results of crawling

9. run: cd ..

10. run: ./manage.py result