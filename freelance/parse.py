import codecs
import xlsxwriter

from selenium import webdriver


PATH_TO_CHROME = r'/usr/lib/chromium-browser/chromedriver'


# class Proxy():
#     @classmethod
#     def close_proxy(cls):
#         bashCommand = "sudo ./hma-vpn.sh -x"
#         os.system(bashCommand)
#     @classmethod
#     def change_pr(cls):
#         bashCommand = "sudo ./hma-vpn.sh -c password.txt -d"
#         os.system(bashCommand)

file = codecs.open("file.txt", 'r', "utf-8")
result = {}
n = 0
driver = webdriver.Chrome(executable_path=PATH_TO_CHROME)
for line in file:
    if not driver:
        driver = webdriver.Chrome(executable_path=PATH_TO_CHROME)
    # Proxy.change_pr()
    line = line.strip('\n')

    driver.get('https://yandex.ua/search/?text=%s&rstr=-213&lr=143' % line)
    links = driver.find_elements_by_xpath("//a[@class='link link_outer_yes path__item']")
    domains = []
    for link in links:
            domain = link.get_attribute("href")
            domains.append(domain.split('//')[1].split('/')[0])
    true_dom = set(domains)
    domains = list(true_dom)
    if n == 0:
        for dom in domains:
            result[str(dom)] = 1
    else:
        for dom in domains:
            try:
                result[str(dom)] += 1
            except Exception, e:
                result[str(dom)] = 1
    n += 1
    # driver.close()
    # Proxy.close_proxy()
print result
workbook = xlsxwriter.Workbook('result.xlsx')
worksheet = workbook.add_worksheet()

row = 0
col = 0
for key, value in result.items():
    worksheet.write(row, col, key)
    worksheet.write(row, col + 1, value)
    row += 1
driver.close()
workbook.close()
