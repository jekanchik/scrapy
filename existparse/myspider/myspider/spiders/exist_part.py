# -*- coding: utf-8 -*-
import scrapy
import time
import selenium
from selenium import webdriver

from scrapy.spiders import CrawlSpider
from carmodal.models import TradeMarkPart


class MySpider(CrawlSpider):
    name = "exist_part"
    allowed_domains = ['exist.ua']

    start_urls = ['http://exist.ua/']

    def parse(self, response):
        driver = webdriver.Firefox()
        driver.get("http://exist.ua/")
        search = driver.find_element_by_name('pcode')
        part = TradeMarkPart.objects.first()
        search.send_keys(part.part_number)
        but = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[1]/form/input[2]').click()
        time.sleep(3)
        info = driver.find_elements_by_class_name('description')
        print '---------------------------------------------------------------'
        for item in info:
            print item.text
        print '---------------------------------------------------------------'
