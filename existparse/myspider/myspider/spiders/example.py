# -*- coding: utf-8 -*-
import importlib
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class MySpider(CrawlSpider):
    name = "spy"
    allowed_domains = ['exist.ua']
    start_urls = ['http://exist.ua/cat/TecDoc/']

    def __init__(self):
        MySpider.rules = [
            Rule(LinkExtractor(allow=[r"/TecDoc/\w*"]),
                 callback='parse_item',
                 follow=True)
        ]
        super(MySpider, self).__init__()

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        for title in response.xpath("//dl[@class='carInfo']/dd"):
            MySpiderItem = getattr(module, 'MySpiderItem')
            carmodel_item = MySpiderItem()
            url = response.url
            carmodel_id = url.split('/')[-1]
            carmodel_item['exist_id'] = carmodel_id
            exist_modification = title.xpath("h1/text()").extract()
            carmodel_item['exist_modification'] = exist_modification[0]
            year_start = title.xpath("div[1]/b[1]/text()").extract()
            year_end = title.xpath("div[1]/b[2]/text()").extract()
            if not title.xpath("div[1]/b[2]/text()").extract():
                year_end = title.xpath("div[1]/i/text()").extract()
            carmodel_item['exist_years'] = year_start[0] + '-' + year_end[0]
            carmodel_item['exist_url'] = response.url
            car_options = title.xpath("div[2]/b/text()").extract()
            car_description = title.xpath("div[2]/text()").extract()
            data = {}
            counter = 0
            for item in car_options:
                data[item] = car_description[counter]
                counter += 1
            carmodel_map = {
                u'Тип двиг.:': 'exist_engine_type',
                u'Модель двиг.:': 'exist_engine_model',
                u'Привод:': 'exist_privod',
                u'Тип КПП:': 'exist_transmission',
                u'Кол-во дверей:': 'exist_doors'
            }
            for key, value in carmodel_map.iteritems():
                if data.get(key, None):
                    carmodel_item[value] = data[key]
            carmodel_item['exist_years']
            return carmodel_item
