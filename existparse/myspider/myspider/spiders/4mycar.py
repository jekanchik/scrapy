# -*- coding: utf-8 -*-
import time
import scrapy
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "formycar"
    allowed_domains = ['4mycar.ru']
    img_path = ".//div[@class='topBlock']/div[2]/div/div/a[1]/img/@src"
    brands_path = ".//div[@class='articleDesc']/h3/text()"
    models_path = ".//div[@class='articleDesc']/div[1]/text()"

    def start_requests(self):
        for i in range(1, 2):
            yield scrapy.Request(
                "https://4mycar.ru/catalog/tires?limit=20&start=" + str(i * 20),
                self.parse_item, dont_filter=True)

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        ForMyCarItem = getattr(module, 'ForMyCarItem')
        # print 'sleep 5 sec'
        # time.sleep(5)
        title = response.xpath("//li[@class='item']").extract()
        print title
        # print response.url
        # print '--------------------------------------------------'
        # print response.headers
        # print '--------------------------------------------------'
        # print response.body
        # raw_images = title.xpath(self.img_path).extract()
        # raw_brands = title.xpath(self.brands_path).extract()
        # raw_models = title.xpath(self.models_path).extract()
        # print raw_brands
        # print raw_models
        # for position, brands in enumerate(raw_brands):
        #     print brands + raw_models[position]
        # models = []
        # for item in raw_models:
        #     item = item.upper().replace('*', '').replace(' ', '')\
        #                        .replace('/', '').replace('-', '')
        #     models.append(item)

        # if len(raw_images) == 20:
        #     images = map(lambda x: 'http:' + x, raw_images)
        #     data_obj = {
        #         'image_urls': images,
        #         'mark': raw_brands,
        #         'model': models
        #     }
        # else:
        #     imageURL = title.xpath("div[@class='topBlock']/div[2]/div")
        #     images_src = imageURL.xpath('.//img/@src').extract()
        #     images_src = map(lambda x: 'http:' + x, images_src)
        #     data_obj = {
        #         'image_urls': images_src,
        #         'mark': raw_brands,
        #         'model': models
        #     }
        # return ForMyCarItem(**data_obj)
