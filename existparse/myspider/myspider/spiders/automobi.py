# -*- coding: utf-8 -*-
import re
import scrapy
import importlib

from scrapy.spiders import CrawlSpider
from carmodal.models import MyCartShiny


class MySpider(CrawlSpider):
    name = "shiny"
    allowed_domains = ['automobi.ua']
    brand_key_path = "td[2]/div/p[1]/a/text()"
    brands_path = "td[2]/div/p[1]/text()"
    models_path = "td[2]/div/p[@class='amount']/text()"

    def jekanchik_generators(self, n):
        i = 1
        while i < n:
            yield i
            i += 1

    def start_requests(self):
        for i in self.jekanchik_generators(178):
            yield scrapy.Request(
                "http://automobi.ua/catalog/tovary/shiny-i-diski/shiny/" +
                "order-price_asc/page-" + str(i), self.parse_item)

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        title = response.xpath("//table[@class='goods']/tr")
        AutomobiItem = getattr(module, 'AutomobiItem')
        brand_key = title.xpath(self.brand_key_path).extract()
        brand = title.xpath(self.brands_path).extract()
        model_bad_view = title.xpath(self.models_path).extract()
        brands = []
        for item in brand:
            item = item.strip(' \n ')
            if item:
                brands.append(item)
        model_list_fake = []
        for position in xrange(0, len(model_bad_view)):
            brands_upper = brands[position].upper()
            model_upper = model_bad_view[position].upper()
            model = re.search(r'(?=%s).*' % brands_upper, model_upper).group()
            model = model.strip('"').split(" ")
            del model[0]
            model_list_fake.append(' '.join(model))
        models = []
        for item in model_list_fake:
            model = item.replace('\\', '/').replace(' / ', '/')\
                        .replace('XL', '').replace('/', '').replace('-', '')
            model = model.split(' ')
            del model[-2:]
            models.append(''.join(model))
        for position, item in enumerate(models):
            if MyCartShiny.objects.filter(model=item):
                tmp = MyCartShiny.objects.filter(model=item)
                qs = tmp.first()
                data_obj = {
                    'image': qs.image,
                    'mark_key': brand_key[position],
                    'mark': brands[position],
                    'model': models[position]
                }
                return AutomobiItem(**data_obj)
