# -*- coding: utf-8 -*-
import re
from scrapy.http import Request
import importlib
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from carmodal.models import FileBus


class MySpider(CrawlSpider):
    name = "shyp_shyna"
    allowed_domains = ['shyp-shyna.com.ua']
    start_urls = ['http://shyp-shyna.com.ua/catalog/' +
                  'bus/cartype-car/']
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.ShypShynaPipeline': 300,
                           },
    }
    href_image_path = ".//td[@class='catalog_section_pic_body']/a/@href"
    key_path = "//table[@class='catalog_element_list']/" +\
               "tr/td[1]/text()"
    href_title_path = "//div[@class='catalog_section_name']/a/@href"
    brand_model_path = "//div[@class='center_col_title']/h1/text()"

    def __init__(self):
        MySpider.rules = [
            Rule(LinkExtractor(allow=[r"/cartype-car/\?PAGEN_9=\d"]),
                 callback='parse_item',
                 follow=True)
        ]
        super(MySpider, self).__init__()

    def parse_item(self, response):
        href_image = response.xpath(self.href_image_path).extract()
        image_urls = map(lambda x: 'http://' + self.allowed_domains[0] +
                         x, href_image)
        href_title = response.xpath(self.href_title_path).extract()
        position = 0
        for url in href_title:
            link = 'http://' + self.allowed_domains[0] + url
            yield Request(link, self.parse_page,
                          meta={'img': image_urls[position]},
                          dont_filter=True)
            position += 1

    def parse_page(self, response):
        module = importlib.import_module('myspider.items')
        ShypShynaItem = getattr(module, 'ShypShynaItem')
        brand_model = response.xpath(self.brand_model_path).extract()[0]\
                                                           .strip('\n\t\r')\
                                                           .strip(' ')\
                                                           .upper()
        if '(' in brand_model:
            count = re.search(r'\(.+\)', brand_model).group()
            brand_model = brand_model.replace(count, '').strip(' ').upper()
        brand_model = brand_model.replace(' ', '')
        length = len(brand_model)
        image_url = []
        image = response.meta['img']
        image_url.append(image)
        keys = response.xpath(self.key_path).extract()
        for key in keys:
            if FileBus.objacts.filter(key=key):
                if FileBus.objacts.filter(image=''):
                    data_obj = {
                        'image_urls': image_url,
                        'title': brand_model,
                        'key': key,
                        'length': length,
                        'parse_from': self.allowed_domains[0]
                    }
                    yield ShypShynaItem(**data_obj)
