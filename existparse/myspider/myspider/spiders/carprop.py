# -*- coding: utf-8 -*-
import scrapy
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "car"
    allowed_domains = ['ukrparts.com.ua']
    title_path = "//div[@class='content']/div[@class='row parts_cats']"
    image_path = "div/a/div[@class='img']/img/@realsrc"
    text = "div/a/div/text()"

    def start_requests(self):
        for i in range(1, 57010):
            yield scrapy.Request(
                "http://ukrparts.com.ua/category/car/" + str(i),
                self.parse_item)

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        for title in response.xpath(self.title_path):
            UkrPropertyItem = getattr(module, 'UkrPropertyItem')
            text = title.xpath(self.text).extract()
            property = []
            for item in text:
                if item.strip('\r\n\t\t\t\t\t'):
                    property.append(item.strip('\r\n\t\t\t\t\t'))

            raw_image = title.xpath(self.image_path).extract()
            images = map(lambda x: self.allowed_domains + x, raw_image)
            url = response.url
            carmodel_id = url.split('/')[-2]
            data_obj = {
                'id': carmodel_id,
                'title': property,
                'image_urls': images
            }
            return UkrPropertyItem(**data_obj)
