# -*- coding: utf-8 -*-
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "upwork"
    allowed_domains = ['www.nhs.uk']
    start_urls = ['http://www.nhs.uk/Services/Trusts/GPs/DefaultView.aspx?id=89652',
                  'http://www.nhs.uk/Services/Trusts/GPs/DefaultView.aspx?id=89817']
    model_path = "//div[@class='col-xs-3']/div[@class='model-name']/a/text()"

    def parse(self, response):
        module = importlib.import_module('myspider.items')
        UpworkItem = getattr(module, 'UpworkItem')
        for result in response.xpath("//div[@class='panel-content']"):
            title = result.xpath("./div/h3/a/text()").extract()
            title_two = result.xpath("./div/dl/dd[1]/text()").extract()
            phone = result.xpath("./div/dl/dd[2]/text()").extract()
            email = result.xpath("./div/dl/dd[3]/a/text()").extract()
            data = {
                'title': title,
                'title_two': title_two,
                'phone': phone,
                'email': email
            }
            print data
            if title:
                yield UpworkItem(**data)
