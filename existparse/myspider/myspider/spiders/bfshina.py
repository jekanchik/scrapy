# -*- coding: utf-8 -*-
import scrapy
import re
import importlib

from scrapy.spiders import CrawlSpider
from carmodal.models import Bus
from collections import Counter


class MySpider(CrawlSpider):
    name = "bfshina"
    allowed_domains = ['bfshina.com.ua']
    item_path = "//div[@class='span-3 catalog_item']"
    brand_model_path = ".//a[@class='catalog_item_title']//text()"
    brand_model_reg = r'(.+(?=\d.* R\d))|(.+(?=\d.* \wR\d))|' +\
                      '.+(?=\d.+\/\d)|.+(?=\d{3} \d{2})'
    image_path = ".//div[@class='catalog_item_img']//img/@src"
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.BfShinaPipeline': 300,
                           },
    }

    def start_requests(self):
        for position in range(1, 8638):
            yield scrapy.Request(
                "http://bfshina.com.ua/avtomobilnye-shiny/page-%s" % position,
                callback=self.parse_item,
                dont_filter=True
            )

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        BfShinaItem = getattr(module, 'BfShinaItem')
        item = BfShinaItem()
        for position, group in enumerate(response.xpath(self.item_path)):
            brand_model = group.xpath(self.brand_model_path).extract()[0]
            if re.search(self.brand_model_reg, brand_model):
                brand_model = re.search(self.brand_model_reg,
                                        brand_model).group()
                brand_model = brand_model.split(' ')
                del brand_model[-1]
                brand_model = ' '.join(brand_model)
                if '(' in brand_model:
                    count = re.search(r' \(.+\)', brand_model).group()
                    brand_model = brand_model.replace(count, '')\
                                             .strip(' ').upper()
                brand_model = brand_model.replace(' ', '').upper()
                if u'РОСАВА' in brand_model:
                    brand_model = brand_model.replace(u'РОСАВА', u'ROSAVA')
                for shiny in Bus.objects.filter(length=len(brand_model),
                                                image_url__isnull=True,
                                                image__isnull=True):
                    if Counter(shiny.brand_model) == Counter(brand_model):
                        image = group.xpath(self.image_path).extract()[0]
                        if not image == '/current/assets/media/nophoto.png':
                            image_url = 'http://' + self.allowed_domains[0] + image
                            images = []
                            images.append(image_url)
                            item['brand_model'] = shiny.brand_model
                            item['image_urls'] = images
                            yield item
