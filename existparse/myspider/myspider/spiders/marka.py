# -*- coding: utf-8 -*-
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "parsemodel"
    allowed_domains = ['ukrparts.com.ua']
    start_urls = ['http://ukrparts.com.ua/category/']
    model_path = "//div[@class='col-xs-3']/div[@class='model-name']/a/text()"

    def parse(self, response):
        module = importlib.import_module('myspider.items')
        MarkaSpiderItem = getattr(module, 'MarkaSpiderItem')
        carmodel_item = MarkaSpiderItem()
        model = response.xpath(self.model_path).extract()
        for i in range(1, 625):
            model_list = model[i].split()
            if model_list[0] == 'Alfa' or model_list[0] == 'Land':
                carmodel_item['marka'] = model_list[0] + ' ' + model_list[1]
                del model_list[0]
                carmodel_item['model'] = model_list
            else:
                carmodel_item['marka'] = model_list[0]
            del model_list[0]
            if len(model_list) == 1:
                carmodel_item['model'] = model_list
            else:
                carmodel_item['model'] = ' '.join(model_list)
            yield carmodel_item
