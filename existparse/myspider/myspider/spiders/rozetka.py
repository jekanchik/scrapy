# -*- coding: utf-8 -*-
import scrapy
import importlib
from scrapy.spiders import CrawlSpider
from carmodal.models import *


class MySpider(CrawlSpider):
    name = "rozetka"
    allowed_domains = ['rozetka.com.ua']
    handle_httpstatus_list = [404, 302]
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.RozetkaImagePipeline': 300,
                           },
    }

    def start_requests(self):
        for car_bus in Bus.objects.filter(image_url__isnull=False,
                                          image__isnull=True):
            req = scrapy.Request(car_bus.image_url_full,
                                 callback=self.check_response,
                                 dont_filter=True)
            yield req

    def check_response(self, response):
        module = importlib.import_module('myspider.items')
        RozetkaImageItem = getattr(module, 'RozetkaImageItem')
        item = RozetkaImageItem()
        if response.status == 200:
            url_full = response.url
            if Bus.objects.filter(image_url_full=url_full, image__isnull=True):
                urls = []
                urls.append(url_full)
                item['url_full'] = url_full
                item['image_urls'] = urls
                item['url'] = 1
                return item
        else:
            if Bus.objects.filter(image_url_full=response.url, image__isnull=True):
                qs_item = Bus.objects.filter(image_url_full=response.url)
                qs = qs_item.first()
                url = []
                url.append(qs.image_url)
                item['url'] = qs.image_url
                item['url_full'] = 1
                item['image_urls'] = url
                return item
