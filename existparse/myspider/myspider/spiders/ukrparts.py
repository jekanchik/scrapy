# -*- coding: utf-8 -*-
import scrapy
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "ukr"
    allowed_domains = ['ukrparts.com.ua']
    start_urls = ['http://ukrparts.com.ua/category/']
    title_path = "//div[@class='car-chooser']"
    extra_data_path = "div[2]/span/text()"
    images_path = "div[3]/a/div/img/@src"

    def start_requests(self):
        for i in range(1, 57010):
            yield scrapy.Request(
                "http://ukrparts.com.ua/category/car/" + str(i),
                self.parse_item)

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        for title in response.xpath(self.title_path):
            UkrSpiderItem = getattr(module, 'UkrSpiderItem')
            url = response.url
            carmodel_id = url.split('/')[-2]
            extra_data = title.xpath(self.extra_data_path).extract()
            extra_data = extra_data[0].strip("\r\n\t") + ' ; ' + extra_data[1]
            raw_images = title.xpath(self.images_path).extract()
            images = map(lambda x: self.allowed_domains + x, raw_images)
            data_obj = {
                'id': carmodel_id,
                'extra_data': extra_data,
                'image_urls': images
            }
            return UkrSpiderItem(**data_obj)
