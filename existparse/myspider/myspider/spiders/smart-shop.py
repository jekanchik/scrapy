# -*- coding: utf-8 -*-
import re
import scrapy
import importlib
from scrapy.spiders import CrawlSpider
from carmodal.models import *

LINK_NEXT_PATH = "//div[@class='child_cat']/a[@class='ch1']/@href"
PAGINATION_PATH = "//ul[@class='pagination']//a[@class='pagenav2']/@href"
LINK_LAST_PATH = "//div[@class='pblock']/div[@class='phead']/a/@href"
REGEX_TO_SIZE = r'Размеры:.+|Размеры:.+|pазмеры:.+|pазмер.:.+|Размеры.+:.+'
DESCRIPTION_PATH = "//div[@itemprop='description']//text()"
PART_NUMBER_PATH = "//div[@itemprop='description']"
SIZE_PATH = "//div[@itemprop='description']"
TITLE_PATH = "//div[@class='ssBox']/h1/text()"


class MySpider(CrawlSpider):
    name = "smart"
    allowed_domains = ['smart-shop.ua']
    start_urls = ['http://www.smart-shop.ua/avtotovary/originalnye-' +
                  'aksessuary-avtoproizvoditeley-original.html']
    data_obj = {}

    def parse(self, response):
        for url in response.xpath(LINK_NEXT_PATH).extract():
            link = 'http://www.' + self.allowed_domains[0] + url
            yield scrapy.Request(link,
                                 callback=self.parse_origin_brand,
                                 dont_filter=True)

    def parse_origin_brand(self, response):
        for url in response.xpath(LINK_NEXT_PATH).extract():
            link = 'http://www.' + self.allowed_domains[0] + url
            yield scrapy.Request(link,
                                 callback=self.parse_model_part,
                                 dont_filter=True)

    def parse_model_part(self, response):
        if not response.xpath("//div[@class='child_cat']"):
            url = response.url
            yield scrapy.Request(url,
                                 callback=self.pagination,
                                 dont_filter=True)
        else:
            for url in response.xpath(LINK_NEXT_PATH).extract():
                link = 'http://www.' + self.allowed_domains[0] + url
                yield scrapy.Request(link,
                                     callback=self.parse_model_part,
                                     dont_filter=True)

    def pagination(self, response):
        pagination_urls = []
        if response.xpath("//ul[@class='pagination']"):
            pagination_urls.append(response.url)
            urls = response.xpath(PAGINATION_PATH).extract()
            for url in urls:
                link = 'http://www.' + self.allowed_domains[0] + url
                pagination_urls.append(link)
            for url in pagination_urls:
                req = scrapy.Request(url,
                                     callback=self.parse_paginate_item,
                                     dont_filter=True)
                yield req
        else:
            req = scrapy.Request(response.url,
                                 callback=self.parse_paginate_item,
                                 dont_filter=True)
            yield req

    def parse_paginate_item(self, response):
        for url in response.xpath(LINK_LAST_PATH).extract():
            link = 'http://www.' + self.allowed_domains[0] + url
            req = scrapy.Request(link,
                                 callback=self.parse_product,
                                 dont_filter=True)
            yield req

    def size_in_title_without_word_size(self, response, shop_item):
        size = response.xpath(SIZE_PATH).extract()
        size[0] = size[0].replace('\r\n', '').replace(u'\xa0', '')
        size = re.search(r'Длина:.+',
                         size[0].encode('utf-8', "ignore")).group()
        if re.search(r'<.+(?=P\wrt|\w.rt)', size):
            size = re.search(r'<.+(?=P\wrt|\w.rt)', size).group()
        size = re.sub(r'\<[^>]*\>', ' ', size)
        if 'либо' in size:
            size = re.findall(r'\d+ см. \d{11}', size)
        for item in size:
            value = item.split(':')
            catalog_number = value[0]
            search_number = catalog_number
            text = re.search(r'.+(?= \(.+\))',
                             shop_item['title'][0]).group()
            value[1] = re.search(r'.+(?= \W)', value[0]).group()
            shop_item['title'][0] = text + u' (' + value[1] + u' см)'
            fake_number = self.check_catalog_number(catalog_number)
            item_data = {'shop_item': shop_item,
                         'catalog_number': catalog_number,
                         'search_number': search_number,
                         'fake_number': fake_number}
            self.compile_data_obj(item_data)

    def compile_data_obj(self, item_data):
        self.data_obj = {
            'title': item_data.get('shop_item')['title'][0],
            'description': item_data.get('shop_item')['description'][0],
            'url': item_data.get('shop_item')['url'],
            'part_number_catalog': item_data.get('catalog_number'),
            'part_number_search': item_data.get('shop_item'),
            'fake_number': item_data.get('fake_number')
        }

    def number_size(self, response, search_number, catalog_number, shop_item):
        text_with_size = response.xpath(SIZE_PATH).extract()[0].replace(
            '\r\n', '').replace(u'\xa0', '')
        if re.search(REGEX_TO_SIZE,
                     text_with_size.encode('utf-8', "ignore")):
            self.size_in_description(text_with_size, shop_item)
        else:
            fake_number = self.check_catalog_number(catalog_number)
            item_data = {'shop_item': shop_item,
                         'catalog_number': catalog_number,
                         'search_number': search_number,
                         'fake_number': fake_number}
            self.compile_data_obj(item_data)

    def size_in_description(self, text_with_size, shop_item):
        size = re.search(REGEX_TO_SIZE,
                         text_with_size.encode('utf-8', "ignore")).group()
        if re.search(r'<.+(?=P\wrt|\w.rt)', size):
            size = re.search(r'<.+(?=P\wrt|\w.rt)', size).group()
        else:
            size = re.search(r':.+', size).group()
            size.strip(':')
        size = re.sub(r'\<[^>]*\>', ' ', size)
        if '(' in size:
            size = re.sub(r'\([^\)]+\)', '', size)
        size = size.replace(' - ', '-').replace('или', '')\
                   .replace(' -', '-').replace('- ', '-')\
                   .strip(' ')
        if 'EU' and 'US' in size:
            self.is_eu_in_size(size, shop_item)
        else:
            self.is_not_eu_in_size(size, shop_item)

    def is_eu_in_size(self, size, shop_item):
        size = size.replace(' ', '')
        size = re.findall(r'(\d{2}-\d{2})(?=\/)|(\d{11})',
                          size)
        key = []
        number = []
        for position, item in enumerate(size):
            if position % 2 == 0:
                number.append(item[0])
            else:
                key.append(item[1])
        size = []
        for position, item in enumerate(number):
            size.append(item + ':' + key[position])
        for item in size:
            value = item.split(':')
            if len(value[0]) < 8:
                self.size_before_number(value, shop_item)
            else:
                self.size_after_number(value, shop_item)

    def is_not_eu_in_size(self, size, shop_item):
        size = size.split(' ')
        number = []
        for item in size:
            item.strip(' ')
        for item in size:
            if not len(item) < 5 and not len(item) > 50:
                number.append(item)
        size = number
        for item in size:
            value = item.split('-')
            catalog_number = value[1]
            search_number = catalog_number
            text = re.search(r'.+(?= \(.+\))',
                             shop_item['title'][0]).group()
            shop_item['title'][0] = text + u' (размер ' + value[0] + u')'
            fake_number = self.check_catalog_number(catalog_number)
            item_data = {'shop_item': shop_item,
                         'catalog_number': catalog_number,
                         'search_number': search_number,
                         'fake_number': fake_number}
            self.compile_data_obj(item_data)

    def size_before_number(self, value, shop_item):
        catalog_number = value[1]
        search_number = catalog_number
        text = re.search(r'.+(?= \(.+\))',
                         shop_item['title'][0]).group()
        shop_item['title'][0] = text + u' (размер ' + value[0] + u')'
        fake_number = self.check_catalog_number(catalog_number)
        item_data = {'shop_item': shop_item,
                     'catalog_number': catalog_number,
                     'search_number': search_number,
                     'fake_number': fake_number}
        self.compile_data_obj(item_data)

    def size_after_number(self, value, shop_item):
        catalog_number = value[0]
        search_number = catalog_number
        text = re.search(r'.+(?= \(.+\))',
                         shop_item['title'][0]).group()
        shop_item['title'][0] = text + u' (размер ' + value[1] + u')'
        fake_number = self.check_catalog_number(catalog_number)
        item_data = {'shop_item': shop_item,
                     'catalog_number': catalog_number,
                     'search_number': search_number,
                     'fake_number': fake_number}
        self.compile_data_obj(item_data)

    def check_catalog_number(self, catalog_number):
        if re.search(r'\.|\/|-| |:|<|>|_|;|=|\+', catalog_number):
            fake_number = False
        else:
            fake_number = True
        return fake_number

    def product_without_size(self, shop_item, catalog_number, search_number):
        fake_number = self.check_catalog_number(catalog_number)
        item_data = {'shop_item': shop_item,
                     'catalog_number': catalog_number,
                     'search_number': search_number,
                     'fake_number': fake_number}
        self.compile_data_obj(item_data)

    def parse_product(self, response):
        module = importlib.import_module('myspider.items')
        SmartItem = getattr(module, 'SmartItem')
        shop_item = {
            'title': response.xpath(TITLE_PATH).extract(),
            'description': response.xpath(DESCRIPTION_PATH).extract(),
            'url': response.url,
            'part_number': response.xpath(PART_NUMBER_PATH).extract()
        }
        if not re.search(r'\wart \wumber.+\.(?=<)',
                         shop_item['part_number'][0]):
            return False
        catalog_number = re.sub(
            r'\<[^>]*\>',
            '',
            re.search(
                r'\wart \wumber.+\.(?=<)',
                shop_item['part_number'][0]
            ).group()
        ).split(':')[-1].strip('.').strip(' ')
        if re.search(r'.+(?=\()', catalog_number):
            catalog_number = re.search(r'.+(?=\()', catalog_number
                                       ).group()
        search_number = catalog_number.replace('.', '').replace(' ', '')\
                                      .replace('-', '').replace('+', '')\
                                      .replace('/', '')
        if u'размер' in shop_item['title'][0]:
            self.number_size(response, search_number,
                             catalog_number, shop_item)
        elif u'см)' in shop_item['title'][0]:
            self.size_in_title_without_word_size(response, shop_item)
        else:
            self.product_without_size(shop_item, catalog_number, search_number)
        data_obj = self.data_obj
        return SmartItem(**data_obj)
