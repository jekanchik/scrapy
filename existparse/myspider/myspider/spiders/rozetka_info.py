# -*- coding: utf-8 -*-
import re
import scrapy
import importlib

from scrapy.spiders import CrawlSpider

from carmodal.models import Bus
from collections import Counter


class MySpider(CrawlSpider):
    name = "rozetka_info"
    allowed_domains = ['rozetka.com.ua']
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.RozetkaInfoPipeline': 300,
                           },
    }
    get_url_path = ".//div[@class='g-i-tile-i-title clearfix']/a/@href"
    get_title_path = "//div[@class='g-i-tile-i-title clearfix']/a/text()"
    title_path = ".//div[@class='detail-title-code pos-fix clearfix']/" +\
                 "h1/text()"
    image_path_full = ".//div[@class='responsive-img']/img/@data-zoom-url"
    image_path = ".//div[@class='responsive-img']/img/@src"
# 2969
    def start_requests(self):
        for position in xrange(2000, 2970):
            yield scrapy.Request(
                "http://rozetka.com.ua/avtoshiny/c4330821/page=%s" % position,
                callback=self.parse_item,
                dont_filter=True)

    def parse_item(self, response):
        urls = response.xpath(self.get_url_path).extract()
        for position, title in enumerate(response.xpath(self.get_title_path).extract()):
            title = title.strip("\r\n\t")
            title = title.strip(" ")
            title = re.search(r'(.+(?=\d.* R\d))|(.+(?=\d.* \wR\d))|(.+(?=\d.*R\d))',
                              title).group()
            title = title.split(" ")
            del title[-1]
            brand = title[0]
            del title[0]
            if title[0] in [u'(НКШЗ)', u'(Киров)', u'(ДШЗ)', u'(ВлТР)']:
                brand = title[0].replace('(', '').replace(')', '')
                del title[0]
            model = " ".join(title)
            brand_model = brand + model
            title = brand_model.replace(' ', '')
            if u'РОСАВА' in title:
                title = title.replace(u'РОСАВА', u'ROSAVA')
            for shiny in Bus.objects.filter(length=len(title),
                                            image_url__isnull=True):
                if Counter(shiny.brand_model) == Counter(title):
                    if shiny.image_url is None:
                        yield scrapy.Request(urls[position],
                                             callback=self.parse_page)

    def get_good_view_title(self, title):
        title = title[0].strip("\r\n\t")
        title = title.strip(" ")
        title = re.search(r'(.+(?=\d.* R\d))|(.+(?=\d.* \wR\d))|(.+(?=\d.*R\d))',
                          title).group()
        title = title.split(" ")
        del title[0]
        del title[-1]
        brand = title[0]
        del title[0]

        if title[0] in [u'(НКШЗ)', u'(Киров)', u'(ДШЗ)', u'(ВлТР)']:
            brand = title[0].replace('(', '').replace(')', '')
            del title[0]
        model = " ".join(title)
        brand_model = brand + model
        title = brand_model.replace(' ', '')
        return title

    def parse_page(self, response):
        module = importlib.import_module('myspider.items')
        RozetkaItem = getattr(module, 'RozetkaItem')
        image_url_full = response.xpath(self.image_path_full).extract()
        image_url = response.xpath(self.image_path).extract()
        title = response.xpath(self.title_path).extract()
        title = self.get_good_view_title(title)
        data_obj = {
            'brand_model': title,
            'image_url_full': image_url_full[0],
            'image_url': image_url[0]
        }
        if len(Bus.objects.filter(brand_model=data_obj['brand_model'],
                                  image_url__isnull=True)) > 0:
            return RozetkaItem(**data_obj)
