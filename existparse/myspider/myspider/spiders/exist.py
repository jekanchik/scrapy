# -*- coding: utf-8 -*-
import re
import scrapy
import importlib
from scrapy.spiders import CrawlSpider


IMAGE_URL = "http://exist.ua/img.exist.ru/img.jpg?Key=%s&MethodType=5"


class MySpider(CrawlSpider):
    name = "exist"
    allowed_domains = ['exist.ua']
    start_urls = ['http://exist.ua/Cat/Cat/Hint/?cat=1']
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.ExistPipeline': 300,
                           },
        'IMAGES_STORE': '/home/jekanchik/cars/',
    }

    def parse(self, response):
        module = importlib.import_module('myspider.items')
        ExistItem = getattr(module, 'ExistItem')
        for column in response.xpath("//div[@class='catalog-column']"):
            href = column.xpath(".//a/@href").extract()
            for position, brand in enumerate(column.xpath(".//a/text()"
                                                          ).extract()):
                url = 'http://exist.ua' + href[position] + '&all=1'
                car_type = href[position].split('=')[-1]
                item = ExistItem()
                item['car_brand'] = brand
                item['car_type'] = car_type
                req = scrapy.Request(url,
                                     meta={'item': item},
                                     callback=self.parse_item,
                                     dont_filter=True)
                yield req

    def parse_item(self, response):
        item = response.request.meta['item']
        car_model = []
        images = []
        body_code = []
        start_date = []
        end_date = []
        for title in response.xpath("//div[@class='carInfo rt']"):
            model_url = title.xpath("dd/div[1]/a/@href").extract()
            model = title.xpath("dd/div[1]/a/text()").extract()[0]
            start = title.xpath("dd/div[3]/b[1]/text()").extract()
            end = title.xpath("dd/div[3]/b[2]/text()").extract()
            car_view_key = title.xpath("dd/div[2]/text()").extract()
            car_id = re.search(r'\d+(?=&type)', model_url[0]).group()
            car_view_key = title.xpath("dd/div[2]/text()").extract()
            images.append(IMAGE_URL % car_id)
            car_model.append(model)
            body_code.append(car_view_key)
            start_date.append(start[0])
            if end:
                end_date.append(end[0])
            else:
                end_date.append(None)
        item['car_model'] = car_model
        item['image_urls'] = images
        item['body_code'] = body_code
        item['start_date'] = start_date
        item['end_date'] = end_date
        return item
