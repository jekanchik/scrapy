# -*- coding: utf-8 -*-
import importlib
from scrapy.spiders import CrawlSpider


class MySpider(CrawlSpider):
    name = "rosa"
    allowed_domains = ['4mycar.ru']
    start_urls = [
        'http://4mycar.ru/catalog/tires?page=goods_catalog&' +
        'goods_group=tires&action=search&viewMode=tile&property%5Bbrands' +
        '%5D%5B%5D=Rosava']
    img_path = "div[@class='topBlock']/div[2]/div"
    brands_path = "div[@class='topBlock']/div[@class='articleDesc']/h3/text()"
    models_path = "div[@class='topBlock']/div[@class='articleDesc']/" +\
                  "div[1]/text()"

    def parse(self, response):
        module = importlib.import_module('myspider.items')
        title = response.xpath("//ul[@class='item_ul']/li")
        MyCarItem = getattr(module, 'MyCarItem')

        raw_images_path = title.xpath(self.img_path)
        raw_brands = title.xpath(self.brands_path).extract()
        raw_models = title.xpath(self.models_path).extract()

        models = []
        for item in raw_models:
            item = item.upper().replace('*', '').replace(' ', '')\
                               .replace('/', '').replace('-', '')
            models.append(item)

        images_src = raw_images_path.xpath('.//img/@src').extract()
        images = map(lambda x: 'http:' + x, images_src)
        data_obj = {
            'image_urls': images,
            'mark': raw_brands,
            'model': models
        }
        return MyCarItem(**data_obj)
