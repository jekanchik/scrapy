# -*- coding: utf-8 -*-
import re
import scrapy
import importlib

from scrapy.spiders import CrawlSpider

from carmodal.models import Bus
from collections import Counter


class MySpider(CrawlSpider):
    name = "rozetka_moto"
    allowed_domains = ['rozetka.com.ua']
    custom_settings = {
        'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1,
                           'myspider.pipelines.RozetkaInfoPipeline': 300,
                           },
    }
    urls_path = ".//div[@class='g-i-tile-i-box-desc']"
    get_url_path = ".//div[@class='g-i-tile-i-title clearfix']/a/@href"
    title_path = ".//div[@class='detail-title-code pos-fix clearfix']/" +\
                 "h1/text()"
    image_path_full = ".//div[@class='responsive-img']/img/@data-zoom-url"
    image_path = ".//div[@class='responsive-img']/img/@src"
# 129
    def start_requests(self):
        for position in range(40, 130):
            yield scrapy.Request(
                "http://rozetka.com.ua/motoshyny/c4626534/page=%s" % position,
                callback=self.parse_item,
                dont_filter=True
            )

    def parse_item(self, response):
        module = importlib.import_module('myspider.items')
        RozetkaItem = getattr(module, 'RozetkaItem')
        item = RozetkaItem()
        urls = []
        for url in response.xpath(self.urls_path):
            urls.append(url.xpath(self.get_url_path).extract())
        item['url'] = urls

        for url in item['url']:
            yield scrapy.Request(url[0], callback=self.parse_page,
                                 dont_filter=True)

    def parse_page(self, response):
        module = importlib.import_module('myspider.items')
        RozetkaItem = getattr(module, 'RozetkaItem')
        title = response.xpath(self.title_path).extract()
        image_url_full = response.xpath(self.image_path_full).extract()
        image_url = response.xpath(self.image_path).extract()
        title = title[0].strip("\r\n\t")
        title = title.strip(" ")
        if re.search(r'.+(?= \d.+\/\d)', title):
            title = re.search(r'.+(?= \d.+\/\d)', title).group()
            title = title.split(" ")
            del title[0]
            title = ''.join(title).upper()
            for shiny in Bus.objects.filter(length=len(title)):
                if Counter(shiny.brand_model) == Counter(title):
                    if shiny.image_url is None:
                        print title
                        data_obj = {
                            'brand_model': shiny.brand_model,
                            'image_url_full': image_url_full[0],
                            'image_url': image_url[0]
                        }
                        return RozetkaItem(**data_obj)
