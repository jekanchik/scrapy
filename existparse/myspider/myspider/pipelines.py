# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from carmodal.models import *
from myspider.items import *


# class SmartShopPipeline(object):
#     def process_item(self, item, spider):
#         product = SmartShop(title=item['title'],
#                             description=item['description'],
#                             part_number_catalog=item['part_number_catalog'],
#                             part_number_search=item['part_number_search'],
#                             fake_number=item['fake_number'],
#                             url=item['url'])
#         product.save()
#         return item


class MySpiderPipeline(object):
    def process_item(self, carmodel_item, spider):
        carmodel_qs = CarModel.objects.filter(id=carmodel_item['exist_id'])
        if carmodel_qs:
            carmodel = carmodel_qs.first()
            carmodel.exist_modification = carmodel_item['exist_modification']
            carmodel.exist_years = carmodel_item['exist_years']
            carmodel.exist_engine_type = carmodel_item.get(
                'exist_engine_type', None)
            carmodel.exist_engine_model = carmodel_item.get(
                'exist_engine_model', None)
            carmodel.exist_transmission = carmodel_item.get(
                'exist_transmission', None)
            carmodel.exist_doors = carmodel_item.get(
                'exist_doors', None)
            carmodel.exist_privod = carmodel_item.get(
                'exist_privod', None)
            carmodel.save()
        return carmodel_item


class SpiderPipeline(object):
    def process_item(self, carmodel_item, spider):
        carmodel_mod = CarModification.objects.filter(id=carmodel_item['id'])
        if carmodel_mod:
            carmodel = carmodel_mod.first()
            carmodel.extra_data = carmodel_item['extra_data']
            carmodel.mod_img = 'car_images/' +\
                carmodel_item['images'][0]['path']
            carmodel.save()
        return carmodel_item


class CarPropertyPipeline(object):
    def process_item(self, property_item, spider):
        for position in range(1, len(property_item['title'])):
            title = property_item['title'][position]
            image = property_item['images'][position]['path']
            car_modif_id = property_item['id']
            carmodel = CarTreeProperty(title=title,
                                       img=image,
                                       car_modif_id=car_modif_id)
            carmodel.save()
        return property_item


class ShinyPipeline(object):
    def process_item(self, shiny_item, spider):
        for position in range(0, len(shiny_item['mark_key'])):
            mark_key = shiny_item['mark_key'][position]
            mark = shiny_item['mark'][position]
            model = shiny_item['model'][position]
            shiny = AutomobyPartShiny(mark_key=mark_key,
                                      mark=mark,
                                      model=model)
            shiny.save()
        return shiny_item


class MyCarPipeline(object):
    def process_item(self, shinu_item, spider):
        for position in range(0, len(shinu_item['mark'])):
            shinu = MyCartShiny(mark=shinu_item['mark'][position],
                                model=shinu_item['model'][position],
                                image=shinu_item['images'][position]['path'])
            shinu.save()
        return shinu_item


class CarShinyImagePipeline(object):
    def process_item(self, shiny_item, spider):
        shiny = CarShinyImage(mark_key=shiny_item['mark_key'],
                              mark=shiny_item['mark'],
                              model=shiny_item['model'],
                              image=shiny_item['image'])
        shiny.save()
        return shiny_item


class ShypShynaPipeline(object):
    def process_item(self, item, spider):
        AllBus.objects.create(key=item['key'],
                              title=item['title'],
                              image=item['images'][0]['path'])
        return item


class RozetkaInfoPipeline(object):
    def process_item(self, item, spider):
        qs = Bus.objects.filter(brand_model=item['brand_model'],
                                image__isnull=True)
        for raw in qs:
            raw.image_url = item['image_url']
            raw.image_url_full = item['image_url_full']
            raw.save()
        return item


class RozetkaImagePipeline(object):
    def process_item(self, item, spider):
        if item['url'] != 1:
            shiny = Bus.objects.filter(image_url=item['url'],
                                       image__isnull=True)
            for item_qs in shiny:
                item_qs.image = item['images'][0]['path']
                item_qs.save(update_fields=['image'])
        else:
            if item['url_full'] != 1:
                shiny = Bus.objects.filter(
                    image_url_full=item['url_full'],
                    image__isnull=True)
                for item_qs in shiny:
                    item_qs.image = item['images'][0]['path']
                    item_qs.save(update_fields=['image'])
        return item


class BfShinaPipeline(object):
    def process_item(self, item, spider):
        print '___________________________________________________________'
        shiny = Bus.objects.filter(brand_model=item['brand_model'],
                                   image__isnull=True)
        for bus in shiny:
            bus.image = item['images'][0]['path']
            bus.save(update_fields=['image'])
        print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'


class ExistPipeline(object):
    def process_item(self, item, spider):
        import os
        for position, group in enumerate(item['images']):
            path = '/home/jekanchik/cars/' + group['path']
            statinfo = os.stat(path)
            if item['body_code'][position]:
                if statinfo.st_size == 1503:
                    os.remove(path)
                    ExistCarBrand.objects.create(car_brand=item['car_brand'],
                                                 car_model=item['car_model'][position],
                                                 car_type=item['car_type'],
                                                 body_code=item['body_code'][position][0],
                                                 start_date=item['start_date'][position],
                                                 end_date=item['end_date'][position])

                else:
                    ExistCarBrand.objects.create(car_brand=item['car_brand'],
                                                 car_model=item['car_model'][position],
                                                 car_type=item['car_type'],
                                                 image=group['path'],
                                                 body_code=item['body_code'][position][0],
                                                 start_date=item['start_date'][position],
                                                 end_date=item['end_date'][position]
                                                 )
            else:
                if statinfo.st_size == 1503:
                    os.remove(path)
                    ExistCarBrand.objects.create(car_brand=item['car_brand'],
                                                 car_model=item['car_model'][position],
                                                 car_type=item['car_type'],
                                                 start_date=item['start_date'][position],
                                                 end_date=item['end_date'][position])

                else:
                    ExistCarBrand.objects.create(car_brand=item['car_brand'],
                                                 car_model=item['car_model'][position],
                                                 car_type=item['car_type'],
                                                 image=group['path'],
                                                 start_date=item['start_date'][position],
                                                 end_date=item['end_date'][position])
        return item
