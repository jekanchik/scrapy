# -*- coding: utf-8 -*-
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class UpworkItem(Item):
    title = Field()
    title_two = Field()
    email = Field()
    phone = Field()


class ExistItem(Item):
    car_brand = Field()
    car_model = Field()
    car_type = Field()
    body_code = Field()
    start_date = Field()
    end_date = Field()
    images = Field()
    image_urls = Field()


class BfShinaItem(Item):
    brand_model = Field()
    images = Field()
    image_urls = Field()
    point = Field()


class SmartItem(Item):
    title = Field()
    description = Field()
    url = Field()
    part_number_catalog = Field()
    part_number_search = Field()
    fake_number = Field()


class RozetkaImageItem(Item):
    images = Field()
    image_urls = Field()
    url_full = Field()
    url = Field()


class RozetkaItem(Item):
    url = Field()
    brand_model = Field()
    image_url = Field()
    image_url_full = Field()


class ForMyCarItem(Item):
    title = Field()
    images = Field()
    image_urls = Field()


class ShypShynaItem(ForMyCarItem):
    key = Field()
    length = Field()
    parse_from = Field()


class AutomobiAllItem(Item):
    mark_key = Field()
    mark = Field()
    model = Field()


class AutomobiItem(AutomobiAllItem):
    image = Field()


class MySpiderItem(Item):
    exist_modification = Field()
    exist_years = Field()
    exist_engine_type = Field()
    exist_engine_model = Field()
    exist_transmission = Field()
    exist_doors = Field()
    exist_privod = Field()
    exist_id = Field()
    exist_url = Field()


class UkrSpiderItem(Item):
    id = Field()
    extra_data = Field()
    images = Field()
    image_urls = Field()


class UkrPropertyItem(Item):
    title = Field()
    image_urls = Field()
    images = Field()
    id = Field()
