# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-24 08:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0156_auto_20160616_1007'),
    ]

    operations = [
        migrations.AddField(
            model_name='trademarkpart',
            name='description',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
    ]
