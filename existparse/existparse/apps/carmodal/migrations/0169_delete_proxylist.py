# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-07-18 15:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0168_proxylist'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProxyList',
        ),
    ]
