# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-26 17:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0032_shypshyna'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rozetka',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('brand', models.CharField(max_length=255, verbose_name='\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c')),
                ('model', models.CharField(max_length=255, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c')),
                ('image', models.ImageField(blank=True, null=True, upload_to='/myspider/rozetka/full/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
        ),
    ]
