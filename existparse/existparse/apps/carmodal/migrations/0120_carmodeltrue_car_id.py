# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-08 11:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0119_carmodeltrue'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodeltrue',
            name='car_id',
            field=models.IntegerField(null=True, verbose_name='\u0418\u0434'),
        ),
    ]
