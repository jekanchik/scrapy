# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-22 08:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0031_delete_shypshynaimage'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShypShyna',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041a\u043e\u0434')),
                ('title', models.CharField(max_length=255, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c')),
                ('image', models.ImageField(blank=True, null=True, upload_to='/myspider/shyp_shyna/full/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
        ),
    ]
