# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-14 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0149_form'),
    ]

    operations = [
        migrations.CreateModel(
            name='FirstForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=255, verbose_name='\u0422\u0435\u043c\u0430 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
                ('message', models.CharField(max_length=255, verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
                ('sender', models.EmailField(max_length=254, verbose_name='Email')),
                ('cc_myself', models.BooleanField(default=False, verbose_name='\u0425\u0417')),
                ('mugshot', models.ImageField(upload_to='/image_form', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
        ),
        migrations.DeleteModel(
            name='Form',
        ),
    ]
