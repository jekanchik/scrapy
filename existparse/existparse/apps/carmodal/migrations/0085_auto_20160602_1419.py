# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-02 14:19
from __future__ import unicode_literals

from django.db import migrations


def true_length_title(apps, schema_editor):
    Bus = apps.get_model("carmodal", "Bus")
    for item in Bus.objects.filter(brand_model__startswith='ROSAVA'):
        item.length = len(item.brand_model)
        item.save(update_fields=['length'])


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0084_auto_20160601_1136'),
    ]

    operations = [
        migrations.RunPython(true_length_title),
    ]
