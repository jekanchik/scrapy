# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-07-13 09:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0158_trademarkpart_inspection'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bus',
            name='image',
        ),
        migrations.RemoveField(
            model_name='bus',
            name='image_url',
        ),
        migrations.RemoveField(
            model_name='bus',
            name='image_url_full',
        ),
        migrations.RemoveField(
            model_name='bus',
            name='new_id',
        ),
    ]
