# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-06 14:03
from __future__ import unicode_literals

from django.db import migrations
import re


def brand(apps, schema_editor):
    ExistCarBrand = apps.get_model("carmodal", "ExistCarBrand")
    for item in ExistCarBrand.objects.all():
        if '(' in item.car_brand:
	        body_code = re.search(r'\(.+\)', item.car_brand).group()
	        item.car_brand = item.car_brand.strip(body_code).strip(' ')
	        item.body_code = body_code.strip('(').strip(')')
	        item.save(update_fields=['car_brand', 'body_code'])


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0101_auto_20160606_1411'),
    ]

    operations = [
        migrations.RunPython(brand),
    ]
