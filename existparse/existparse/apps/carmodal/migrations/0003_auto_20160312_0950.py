# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-12 09:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0002_carmodel_prod_img'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodel',
            name='exist_doors',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0434\u0432\u0435\u0440\u0435\u0439'),
        ),
        migrations.AddField(
            model_name='carmodel',
            name='exist_engine_model',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f'),
        ),
        migrations.AddField(
            model_name='carmodel',
            name='exist_engine_type',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0422\u0438\u043f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f'),
        ),
        migrations.AddField(
            model_name='carmodel',
            name='exist_modification',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='carmodel',
            name='exist_transmission',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0422\u0438\u043f \u041a\u041f\u041f'),
        ),
        migrations.AddField(
            model_name='carmodel',
            name='exist_years',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430-\u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f                                     \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430'),
        ),
        migrations.AlterField(
            model_name='carmodel',
            name='prod_img',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041f\u0443\u0442\u044c \u043a \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0435'),
        ),
    ]
