# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-05 09:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0050_auto_20160505_0950'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Rozetka',
        ),
    ]
