# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-06 15:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0107_auto_20160606_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodel',
            name='body_code',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0422\u0438\u043f \u043a\u0443\u0437\u043e\u0432\u0430'),
        ),
    ]
