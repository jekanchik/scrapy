# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-10 11:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0147_buss'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodelneedimage',
            name='name_true',
            field=models.CharField(max_length=255, null=True, verbose_name='\u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0430'),
        ),
    ]
