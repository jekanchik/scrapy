# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-03 11:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0086_auto_20160602_1445'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('title_rus', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043d\u0430 \u0440\u0443\u0441\u0441\u043a\u043e\u043c')),
                ('slug', models.SlugField(blank=True, max_length=280, null=True, unique=True, verbose_name='URL')),
                ('start_date', models.DateField(blank=True, null=True, verbose_name='\u0413\u043e\u0434 \u043d\u0430\u0447\u0430\u043b\u0430 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('end_date', models.DateField(blank=True, null=True, verbose_name='\u0413\u043e\u0434 \u043a\u043e\u043d\u0446\u0430 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('is_passenger', models.BooleanField(default=False, verbose_name='\u041f\u0430\u0441\u0441\u0430\u0436\u0438\u0440\u0441\u043a\u0438\u0439')),
                ('is_commercial', models.BooleanField(default=False, verbose_name='\u041a\u043e\u043c\u043c\u0435\u0440\u0447\u0435\u0441\u043a\u0438\u0439')),
                ('is_axis_manufacturer', models.BooleanField(default=False, verbose_name='\u041e\u0441\u044c')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='car_brand_logos', verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430')),
                ('is_shown', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c')),
                ('to_main', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
            ],
        ),
    ]
