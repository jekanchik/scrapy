# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-08-04 09:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0180_delete_proxylisttwo'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProxyListTwo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sity', models.CharField(max_length=255, null=True, verbose_name='\u041f\u0440\u043e\u043a\u0441\u0438 \u0441\u0435\u0440\u0432\u0435\u0440')),
                ('ip_address', models.CharField(max_length=255, null=True, verbose_name='\u041f\u0440\u043e\u043a\u0441\u0438 \u0441\u0435\u0440\u0432\u0435\u0440')),
            ],
        ),
    ]
