# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-14 13:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0022_auto_20160413_1858'),
    ]

    operations = [
        migrations.AddField(
            model_name='mycartshiny',
            name='mark_key',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u0442\u043e\u0432\u0430\u0440\u0430'),
        ),
        migrations.AlterField(
            model_name='mycartshiny',
            name='mark',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c'),
        ),
        migrations.AlterField(
            model_name='mycartshiny',
            name='model',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c'),
        ),
    ]
