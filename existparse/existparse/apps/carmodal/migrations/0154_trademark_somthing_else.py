# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-16 10:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carmodal', '0153_trademark_somthing'),
    ]

    operations = [
        migrations.AddField(
            model_name='trademark',
            name='somthing_else',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0427\u0442\u043e-\u0442\u043e \u043e\u0447\u0435\u043d\u044c \u0432\u0430\u0436\u043d\u043e\u0435'),
        ),
    ]
