import re

from django.core.management.base import BaseCommand

from carmodal.models import ShypShyna


class Command(BaseCommand):
    def handle(self, *args, **options):
        images = []
        for objects in ShypShyna.objects.all():
            name = re.search(r'(?=\/).*', objects.image.name).group()
            name = name.strip('/')
            images.append(name)
        import os
        images_in_dir = []
        path = '/home/jekanchik/scrapy_education/existparse/' +\
               'myspider/shyp_shina/full/'
        for filename in os.listdir(path):
            images_in_dir.append(filename)
        list3 = list(set(images_in_dir) - set(images))
        images = map(lambda x: path + x, list3)
        for one in images:
            os.remove(one)
