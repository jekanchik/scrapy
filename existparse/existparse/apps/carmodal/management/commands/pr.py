import time
import signal
from django.core.management.base import BaseCommand
from selenium import webdriver
from selenium.webdriver.common.proxy import *
from carmodal.models import ProxyList


class Command(BaseCommand):
    big_table_path = "//div[@id='proxylisttable_length']//option"
    ip_path = "//table[@id='proxylisttable']//tr/td[1]"
    port_path = "//table[@id='proxylisttable']//tr/td[2]"

    def get_free_proxy(self):
        proxy_list = []
        driver = webdriver.Firefox()
        driver.get("https://free-proxy-list.net/")
        driver.find_element_by_name("proxylisttable_length").click()
        time.sleep(2)
        driver.find_elements_by_xpath(self.big_table_path)[2].click()
        ip = driver.find_elements_by_xpath(self.ip_path)
        port = driver.find_elements_by_xpath(self.port_path)
        for position, item in enumerate(ip):
            pt = port[position].text
            proxy = item.text + ':' + pt
            proxy_list.append(proxy)
        driver.quit()
        return proxy_list

    def err_log(signum, frame, driver):
        print 'timeout > 40 sec'
        raise IOError("Bad proxy")

    def decor(func):
        def wrapper(self, driver):
            print 'decorator'
            signal.signal(signal.SIGALRM, self.err_log)
            signal.alarm(40)
            func(self, driver)
            signal.alarm(0)
        return wrapper

    @decor
    def check_proxy_server(self, driver):
        try:
            driver.get("http://exist.ua/")
        except IOError:
            print 'bad proxy'

    def handle(self, *args, **options):
        myProxy = self.get_free_proxy()
        print myProxy
        for item in myProxy:
            chrome_option = webdriver.ChromeOptions()
            chrome_option.add_argument("--proxy-server=%s" % item)
            self.driver = webdriver.Chrome(chrome_options=chrome_option)
            self.check_proxy_server(self.driver)
            try:
                if self.driver.find_element_by_name('pcode'):
                    if not ProxyList.objects.filter(proxy=item):
                        qs = ProxyList(proxy=item)
                        qs.save()
                        print 'good proxy'
                    else:
                        print 'exist'
                    self.driver.quit()
                else:
                    print 'bad proxy'
                    self.driver.quit()
            except:
                print 'bad proxy'
                self.driver.quit()
