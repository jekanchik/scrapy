# -*- coding: utf-8 -*-
import re

from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        file = open("nomenclatures.csv", 'r')
        for line in file:
            line = line.decode('cp1251').encode('utf-8')
            raw = line.split(';')
            title = raw[5].strip('"').replace('XL', '')
            title = title.split('""')
            title = title[-1]
            title = title.split(' ')
            del title[-2:]
            if len(title) > 0:
                del title[0]
                title = ' '.join(title)

                title = title.replace('зимова', '')\
                             .replace("`", '')\
                             .replace("'", '')\
                             .replace('’', '')\
                             .replace('-', '')\
                             .replace('*', '')\
                             .replace('\\', '/')\
                             .upper()
                if '(' in title:
                    brecks = re.search(r'(\(.*\))', title).group()
                    title = title.replace(brecks, '').strip(' ')
                title = title.replace(' ', '')

                if RozetkaInfo.objects.filter(model=title):
                    qs = RozetkaInfo.objects.filter(model=title)
                    qs = qs.first()
                    if not RozetkaFiltered.objects.filter(id_brand=raw[6]):
                        if not ShypShyna.objects.filter(key=raw[1]):
                            print qs.model, raw[6]
                            shina = RozetkaFiltered(brand=qs.brand,
                                                    model=qs.model,
                                                    image_url=qs.image_url,
                                                    image_url_full=qs.image_url_full,
                                                    id_brand=raw[6],
                                                    key_brand=raw[1]
                                                    )
                            shina.save()
