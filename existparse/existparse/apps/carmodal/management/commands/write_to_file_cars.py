# -*- coding: utf-8 -*-
import unicodecsv as csv
from shutil import copyfile

from django.core.management.base import BaseCommand
from carmodal.models import CarModelNeedImage


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('car_image.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter=';')
        for car_model in CarModelNeedImage.objects.filter(image__isnull=False):
            if car_model.body_code:
                code = u'(' + car_model.body_code + u')'
            else:
                code = ''
            if car_model.start_date:
                st = str(car_model.start_date)
            else:
                st = u'---'
            if car_model.end_date:
                en = str(car_model.end_date)
            else:
                en = u'---'
            true_name = u'full/' + car_model.car_brand + u'_' + car_model.car_model.replace('/', '_') + code.replace('/', '_') + u'_' + st + u'_' + en + u'_' + str(car_model.car_id) + u'.jpg'
            if car_model.image.name:
                writer.writerow([car_model.car_id,
                                 true_name,
                                 car_model.comments])
            else:
                comm = 'Нет такой модели авто. Нужно уточнить название моделя для сопоставления с exist.ua'
                writer.writerow([car_model.car_id, None, comm])
            path = '/home/jekanchik/cars/' + car_model.image.name if car_model.image.name else ''
            new = '/home/jekanchik/cars_all/' + true_name
            print path, new
            copyfile(path, new)
            # try:
            #     copyfile(path, new)
            # except:
            #     raise Exception(car_model.id)
        to_file.close()
