from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        for car_bus in ShypShyna.objects.all():
            model_bus = car_bus.title
            model_bus = model_bus.split(' ')
            del model_bus[:2]
            model_bus = ' '.join(model_bus)
            model_bus = model_bus.upper().replace('\\', '/')\
                                         .replace('-', '')\
                                         .replace(' ', '')\
                                         .replace('*', '')
            car_bus.model = model_bus
            print model_bus
            car_bus.save(update_fields=['model'])
