# -*- coding: utf-8 -*-
import unicodecsv as csv
from django.core.management.base import BaseCommand

from carmodal.models import FileBus


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('Shiny_image.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter=';')
        writer.writerow(['id',
                         'image'
                         ])
        for bus in FileBus.objects.filter(image__isnull=False):
            writer.writerow([bus.id_key,
                             bus.image])
        to_file.close()
