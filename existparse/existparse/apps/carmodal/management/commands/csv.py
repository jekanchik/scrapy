import unicodecsv as csv

from django.core.management.base import BaseCommand
from carmodal.models import CarShinyImage


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('image_bus.csv', 'w+')
        writer = csv.writer(to_file, encoding='cp1251', delimiter=';')

        for car_model in CarShinyImage.objects.all():
                writer.writerow([car_model.id_carbrand, car_model.image.name])
        to_file.close()
