# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db.models import Q
from carmodal.models import ExistCarBrand
from carmodal.models import CarModelNeedImage
from collections import Counter


class Command(BaseCommand):
    def get_car_clear_model(self, item):
        if u'платформой/ходовая' in item.car_model:
            car_model = item.car_model.replace(u'/ходовая часть', '')
        elif u'VOLGA' in item.car_model:
            car_model = item.car_model.replace(u'VOLGA', u'Волга')
        elif u'Наклонная' in item.car_model:
            car_model = item.car_model.replace(u'Наклонная задняя часть',
                                               u'хэтчбек')
        elif u'DEVILLE' in item.car_model:
            car_model = item.car_model.replace(u'DEVILLE', u'DE VILLE')
        elif u'тарга' in item.car_model:
            car_model = item.car_model.replace(u'тарга', u'Targa')
        elif u'Hatchback' in item.car_model:
            car_model = item.car_model.replace(u'Hatchback', u'хэтчбек')
        else:
            car_model = item.car_model.split(' ')
            del car_model[-1]
            car_model = ' '.join(car_model)
        return car_model

    def handle(self, *args, **options):
        car_brand_replace = {u'Askam (Fargo/Desoto)': u'Askam',
                             u'BMW (Brilliance)': u'BMW',
                             u'Buick (SGM)': u'Buick',
                             u'Changan (Chana)': u'Changan',
                             u'Chevrolet (SGM)': u'Chevrolet',
                             u'Citroen (Df-Psa)': u'Citroen',
                             u'Dongnan (Soueast)': u'Dongnan',
                             u'Emgrand (Geely)': u'Geely',
                             u'Englon (Geely)': u'Geely',
                             u'Gleagle (Geely)': u'Geely',
                             u'Haima (FAW)': u'Haima',
                             u'Haima (Zhengzhou)': u'Haima',
                             u'Honda (GAC)': u'Honda',
                             u'Hyundai (Beijing)': u'Hyundai',
                             u'Jinbei (Brilliance)': u'Jinbei',
                             u'Kia (Dyk)': u'Kia',
                             u'Landwind (JMC)': u'Landwind',
                             u'Lotus (Youngman)': u'Lotus',
                             u'Mazda (Changan)': u'Mazda',
                             u'Mazda (FAW)': u'Mazda',
                             u'Mercedes-Benz (Fjda)': u'Mercedes',
                             u'Mercedes-Benz': u'Mercedes',
                             u'Mitsubishi (Soueast)': u'Mitsubishi',
                             u'Peugeot (Df-Psa)': u'Peugeot',
                             u'Pyeonghwa (Pmc)': u'Pyeonghwa',
                             u'Sinotruk (Cnhtc)': u'Sinotruk',
                             u'Suzuki (Changan)': u'Suzuki',
                             u'Tata (Telco)': u'Tata',
                             u'Toyota (FAW)': u'Toyota',
                             u'Toyota (GAC)': u'Toyota',
                             u'Volvo (Changan)': u'Volvo',
                             u'VW (SVW)': u'Volkswagen',
                             u'VW': u'Volkswagen',
                             u'Wuling (SGMW)': u'Wuling',
                             u'Zhonghua(Brilliance)': u'Zhonghua',
                             u'Zhongxing (Zte)': u'Zhongxing',
                             u'ZAZ': u'ЗАЗ',
                             u'GAZ': u'ГАЗ',
                             u'Lada': u'ВАЗ'
                             }
        for item in CarModelNeedImage.objects.filter(error__isnull=True):
            check = 0
            final_item = 0
            if u'Motorcycles' in item.car_brand:
                item.car_brand = item.car_brand.replace(u' Motorcycles', '')
            if item.car_brand in car_brand_replace:
                item.car_brand = car_brand_replace[item.car_brand]
            query = start_query = Q(car_brand__iexact=item.car_brand)
            code_check = Q(id__isnull=False)
            if item.body_code:
                for code in item.body_code.split(','):
                    code_check &= Q(body_code__contains=code)
            else:
                code_check = Q(car_model__iexact=item.car_model)
            query &= Q(code_check)
            qs = ExistCarBrand.objects.filter(query)

            if qs.count() != 1:
                query = start_query if qs.count() == 0 else query

                car_model = self.get_car_clear_model(item)
                if item.end_date:
                    end_year = item.end_date.year
                else:
                    # print 'end_year is None'
                    end_year = None
                if item.start_date:
                    start_year = item.start_date.year
                else:
                    # print 'start_year is None'
                    start_year = None
                query &= Q(car_model__iexact=car_model,
                           start_date=start_year,
                           end_date=end_year)
                qs = ExistCarBrand.objects.filter(query)
            else:
                final_item = qs[0]
            if qs.count() > 1:
                print 'more one qs'
                final_item = qs.first()
                check = 'Нужна проверка'

            elif qs.count() == 0:
                query = start_query if qs.count() == 0 else query
                if start_year is not None:
                    query &= Q(car_model__icontains=car_model,
                               start_date=start_year,
                               end_date=end_year)
                else:
                    query &= Q(car_model__icontains=car_model)
                qs = ExistCarBrand.objects.filter(query).exclude(car_model__iexact=car_model)
                if qs.count() == 0:
                    for qs in ExistCarBrand.objects.filter(car_brand__iexact=item.car_brand,
                                                          length=len(car_model)):
                        if Counter(qs.car_model) == Counter(car_model):
                            final_item = qs
                elif qs.count() > 1:
                    final_item = qs.first()
                    check = 'Нужна проверка'
                else:
                    final_item = qs[0]
            if final_item != 0:
                if not final_item.image == '':
                    if not check == 0:
                        item.image = final_item.image
                        item.comments = 'Нужна проверка может не совпадать'
                        item.save(update_fields=['image', 'comments'])
                    else:
                        item.comments = None
                        item.image = final_item.image
                        item.save(update_fields=['image'])
                else:
                    item.image = None
                    item.comments = 'Нет картинки на exist.ua'
                    item.save(update_fields=['image', 'comments'])
            else:
                item.image = None
                item.comments = 'Нет такой модели авто, или нужно уточнить название моделя для сопоставления с exist.ua'
                item.save(update_fields=['image', 'comments'])
