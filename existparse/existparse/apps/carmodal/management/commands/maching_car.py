from django.core.management.base import BaseCommand

from carmodal.models import CarModel, ExistCarBrand


class Command(BaseCommand):
    def handle(self, *args, **options):
        for position, item in enumerate(ExistCarBrand.objects.all()):
            if CarModel.objects.filter(car_type_shot_key=item.car_model):
                print position, item.car_model
