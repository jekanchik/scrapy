# -*- coding: utf-8 -*-
import time
from timeout import timeout
import os
import psycopg2
import datetime
import zipfile
from selenium.webdriver.chrome.options import Options
from existparse.settings import PROXY_USERNAME, PROXY_PASSWORD, DATABASES, PATH_TO_CHROME
import selenium
from django.utils.timezone import utc
from selenium.webdriver.common.proxy import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from django.core.management.base import BaseCommand
from carmodal.models import TradeMarkPart, ProxyHMA


def get_connection(database='default'):
    connection_opts = {'name': DATABASES['default']['NAME'],
                       'user': DATABASES['default']['USER'],
                       'host': DATABASES['default']['HOST'],
                       'pass': DATABASES['default']['PASSWORD'],
                       }
    conn = psycopg2.connect("dbname = %(name)s user = %(user)s host = %(host)s password = %(pass)s" % connection_opts)
    return conn

def dir_exist():
    if not os.path.exists('/home/jekanchik/exist_captcha/'):
        os.mkdir('/home/jekanchik/exist_captcha/')


class Command(BaseCommand, Proxy):
    recaptcha_path = "//div[@class='g-recaptcha']"
    search_path = '/html/body/div[1]/div[1]/div[2]/div[1]/form/input[2]'
    description_path = "//div[@class='description']"
    captcha_path = "//div[@class='rc-imageselect-desc-no-canonical']"
    image_captcha_path = '/home/jekanchik/exist_captcha/captcha.jpeg'
    class_title = "rc-imageselect-desc-no-canonical"
    table_path = "//div[@id='ctl00_ctl00_b_b_updPrice']/div/table/tbody/tr"
    big_table_path = "//div[@id='proxylistTwotable_length']//option"
    ip_path = "//table[@id='proxylisttable']//tr/td[1]"
    port_path = "//table[@id='proxylisttable']//tr/td[2]"
    driver = None

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path=PATH_TO_CHROME)
        super(Command, self).__init__()

    def save_description(self, part_id, title, mark):
        if u"\'" in title:
            title = title.replace(u"\'", '')
        elif u"\"" in title:
            title = title.replace(u"\"", '')
        elif u"%" in title:
            title = title.replace(u"%", ' o/o')
        print title
        query = """UPDATE carmodal_trademarkpart
                SET description='%(title)s'
                WHERE id=%(part_id)s
                """ % {'title': title, 'part_id': int(part_id)}
        with get_connection() as c:
            cur = c.cursor()
            cur.execute(query, {})
            c.commit()
        print 'Saved data in description. id of position is: %s' % part_id

    def else_name_tm(self, part, firmname, trade_m):
        if firmname in [u'Kia', u'Hyundai']:
            firmname = u'Hyundai / Kia'
        if firmname in [u'Lexus']:
            firmname = u'Toyota'
        if firmname in [u'Lexus']:
            firmname = u'Toyota'
        if firmname == u'VAG':
            firmname = trade_m
        return firmname

    def description_in_table(self, part_id, trade_m, part):
        for item in self.driver.find_elements_by_xpath(self.table_path):
            title = item.text.split('\n')[2]
            firmname = item.text.split('\n')[0].upper()
            if trade_m.upper() == firmname:
                mark = firmname
                self.save_description(part_id, title, mark)
            else:
                firmname = self.else_name_tm(part, firmname, trade_m)
                if trade_m.upper() == firmname.upper():
                    mark = firmname.upper()
                    self.save_description(part_id, title, mark)
                else:
                    if u'(' in trade_m:
                        tm = trade_m.split('(')[0].strip(' ')
                        if tm.upper() == firmname:
                            mark = firmname
                            self.save_description(part_id, title, mark)
                        else:
                            tm = trade_m.split('(')[1].strip(')')
                            if tm.upper() == firmname:
                                mark = firmname
                                self.save_description(part_id, title, mark)

    @timeout(40)
    def check_proxy_server(self):
        self.driver.get("http://exist.ua/")

    def create_zip_with_js(self, query):
        proxy = str(query.proxy).split(':')[0].strip('\t\n\r')
        port = str(query.proxy).split(':')[1].strip('\t\n\r')
        manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Chrome Proxy",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"20.0.0"
        }
        """

        background_js = """
        var config = {
                mode: "fixed_servers",
                rules: {
                  singleProxy: {
                    scheme: "http",
                    host: "%(proxy)s",
                    port: parseInt("%(port)s")
                  },
                  bypassList: ["foobar.com"]
                }
              };

        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "%(user)s",
                    password: "%(password)s"
                }
            };
        }

        chrome.webRequest.onAuthRequired.addListener(
                    callbackFn,
                    {urls: ["<all_urls>"]},
                    ['blocking']
        );
        """ % ({'proxy': proxy, 'port': port,
                'user': PROXY_USERNAME, 'password': PROXY_PASSWORD})

        pluginfile = 'proxy_auth_plugin.zip'

        with zipfile.ZipFile(pluginfile, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)
        print 'good zip complate'
        return pluginfile

    def check_proxy(self, query):
        prev_proxy = query.proxy
        co = Options()
        co.add_argument("--proxy-server=%s" % query.proxy)
        self.driver = webdriver.Chrome(chrome_options=co)
        self.check_proxy_server()
        try:
            self.driver.find_element_by_name('pcode')
            print 'good proxy'
            query.start_date = datetime.datetime.utcnow().replace(tzinfo=utc)
            query.status = True
            query.save(update_fields=['status', 'start_date'])
        except:
            print 'bad proxy'
            self.driver.quit()
            query.status = False
            query.end_date = datetime.datetime.utcnow().replace(tzinfo=utc)
            query.save(update_fields=['status', 'end_date'])
            self.change_proxy(prev_proxy)
        self.parse_page(prev_proxy)

    def get_next_proxy(self, qs, query_last):
        if not qs.id == query_last.id:
            next_proxy = ProxyListTwo.objects.filter(id__gt=qs.id).first()
            if next_proxy.end_date is not None:
                time_now = datetime.datetime.utcnow().replace(tzinfo=utc)
                div_time = time_now - next_proxy.end_date
                hour = str(div_time).split('.')[0].split(':')[0]
                if hour < 12:
                    qs = next_proxy
                    self.get_next_proxy(qs, query_last)
                else:
                    self.check_proxy(next_proxy.proxy)

            else:
                query = ProxyListTwo.objects.get(proxy=next_proxy.proxy)
                self.check_proxy(query)
        else:
            self.change_proxy(prev_proxy=None)

    def change_proxy(self, prev_proxy):
        if prev_proxy is not None:
            query_last = ProxyListTwo.objects.last()
            qs = ProxyListTwo.objects.filter(proxy=prev_proxy)[0]
            if not qs.id == query_last.id:
                self.get_next_proxy(qs, query_last)
            else:
                query = ProxyListTwo.objects.first()
                tome_now = datetime.datetime.utcnow().replace(tzinfo=utc)
                div_time = tome_now - query.end_date
                print 'div time: ', (str(div_time)).split('.')[0]
                hour = str(div_time).split('.')[0].split(':')[0]
                if int(hour) > 12:
                    self.check_proxy(query)
                else:
                    div = 12 - hour
                    print 'sleep %s hour' % div
                    time_sleep = div * 3600
                    time.sleep(time_sleep)
                    query = ProxyListTwo.objects.first()
                    self.check_proxy(query)
        else:
            query = ProxyListTwo.objects.filter(start_date__isnull=False,
                                                end_date__isnull=True).first()
            if query:
                self.check_proxy(query)
            else:
                query = ProxyListTwo.objects.filter(start_date__isnull=True,
                                                    end_date__isnull=True).first()
                if query:
                    self.check_proxy(query)
                else:
                    print 'not such query'
                    query = ProxyListTwo.objects.first()
                    self.check_proxy(query)

    def save_time(self, prev_proxy):
        qs = ProxyListTwo.objects.filter(proxy=prev_proxy)[0]
        qs.end_date = datetime.datetime.utcnow().replace(tzinfo=utc)
        qs.save(update_fields=['end_date'])

    def h2_is_exist(self, part_id):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, "//div[@id='ajaxupdatepanel']//h2"))
            )
            is_exist = True
        except selenium.common.exceptions.WebDriverException:
            is_exist = False
        return is_exist

    def check_err(fn):
        def wrapped(self):
            try:
                fn(self)
            except Exception as e:
                print e
                time.sleep(10)
                obj = Proxy()
                obj.close_proxy()
                self.handle()

        return wrapped

    @check_err
    def parse_page(self, prev_proxy=None, p=None):
        query = """
            SET STATEMENT_TIMEOUT TO 0; COMMIT;
            """
        with get_connection() as c:
            cur = c.cursor()
            cur.execute(query, {})
        self.driver.delete_all_cookies()
        self.driver.get("http://exist.ua/")

        query = """SELECT "carmodal_trademarkpart"."id", "carmodal_trademarkpart"."part_number", "carmodal_trademark"."tm"
            FROM "carmodal_trademarkpart"
            INNER JOIN "carmodal_trademark"
            ON ("carmodal_trademarkpart"."trade_mark_id" = "carmodal_trademark"."car_id")
            WHERE ("carmodal_trademarkpart"."description" IS NULL AND "carmodal_trademarkpart"."inspection" IS NULL)
            LIMIT 500
            """
        res = []

        with get_connection().cursor() as c:
            c.execute(query, {})
            res = c.fetchall()
        id_list = []
        tm = []
        part_number = []
        for position, item in enumerate(res):
            id_list.append(item[0])
            tm.append(item[2])
            part_number.append(item[1])
        qs_dict = {'id': id_list, 'tm': tm, 'part_number': part_number}
        for position, part in enumerate(qs_dict['part_number']):
            part_id = qs_dict['id'][position]
            trade_m = qs_dict['tm'][position]
            print part_id, part, trade_m
            try:
                search = self.driver.find_element_by_id('pcode')
            except selenium.common.exceptions.NoSuchElementException:
                print 'Wait visibility_of_element_located'
                WebDriverWait(self.driver, 20).until(
                    EC.visibility_of_element_located((By.ID, 'pcode'))
                )
            search.send_keys('')
            search.send_keys(part)
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, self.search_path))
            )
            self.driver.find_element_by_xpath(self.search_path).click()
            is_exist = self.h2_is_exist(part_id)
            if is_exist is False:
                try:
                    h1_path = "//div[@id='ajaxupdatepanel']//h1"
                    WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, h1_path))
                    )
                except selenium.common.exceptions.WebDriverException:
                    self.driver.quit()
                    obj = Proxy()
                    obj.close_proxy()
                    obj.change_pr()
                    self.__init__()
                    self.parse_page()
                if not self.driver.find_elements_by_xpath(self.description_path):
                    if self.driver.find_elements_by_xpath(self.recaptcha_path):
                        self.driver.quit()
                        obj = Proxy()
                        obj.close_proxy()
                        obj.change_pr()
                        self.__init__()
                        self.parse_page()
                    elif self.driver.find_elements_by_xpath(self.table_path):
                        self.description_in_table(part_id, trade_m, part)
                else:
                    if self.driver.find_elements_by_xpath(self.description_path):
                        info = self.driver.find_elements_by_xpath(self.description_path
                                                                  )[1]
                        firmname = self.driver.find_elements_by_xpath(
                            "//span[@class='caname']/a")[0]
                        if info:
                            if firmname.text.upper() == trade_m.upper():
                                mark = trade_m
                                title = info.text
                                self.save_description(part_id, title, mark)
                            else:
                                if firmname.text.upper() == u'VAG':
                                    mark = trade_m
                                    title = info.text
                                    self.save_description(part_id, title, mark)
                    else:
                        print 'not found'
                inspection = 'not found on exist.ua'
                query = """UPDATE carmodal_trademarkpart
                        SET inspection='%(inspection)s'
                        WHERE ("carmodal_trademarkpart"."description" IS NULL AND "carmodal_trademarkpart"."id" = %(part_id)s AND "carmodal_trademarkpart"."inspection" IS NULL)
                        """ % {'inspection': inspection, 'part_id': part_id}
                if query:
                    with get_connection() as c:
                        cur = c.cursor()
                        cur.execute(query, {})
                        c.commit()
            else:
                inspection = 'not found on exist.ua'
                query = """UPDATE carmodal_trademarkpart
                        SET inspection='%(inspection)s'
                        WHERE ("carmodal_trademarkpart"."description" IS NULL AND "carmodal_trademarkpart"."id" = %(part_id)s AND "carmodal_trademarkpart"."inspection" IS NULL)
                        """ % {'inspection': inspection, 'part_id': part_id}
                res = []
                if query:
                    with get_connection() as c:
                        cur = c.cursor()
                        cur.execute(query, {})
                        c.commit()
            print '------------------------------------------------------------------------------------------------'
        self.parse_page()
        self.driver.quit()

    def handle(self, *args, **options):
        obj = Proxy()
        obj.change_pr()
        self.parse_page()


class Proxy():
    ip_proxy = None
    browser = webdriver.Chrome(executable_path=PATH_TO_CHROME)

    def close_proxy(self):
        bashCommand = "sudo ./hma-vpn.sh -x"
        os.system(bashCommand)
        time.sleep(10)
        query = """UPDATE carmodal_proxyhma
                SET timest='%(time)s'
                WHERE ip='%(ip)s';
                """ % {'time':datetime.datetime.now(), 'ip': self.ip_proxy}
        with get_connection() as c:
            cur = c.cursor()
            cur.execute(query, {})
            c.commit()

    def change_pr(self):
        if not self.browser:
            browser = webdriver.Chrome(executable_path=PATH_TO_CHROME)
        bashCommand = "sudo ./hma-vpn.sh -c password.txt -d"
        os.system(bashCommand)
        time.sleep(10)
        self.browser.get("http://whatismyipaddress.com/")
        ip = self.browser.find_element_by_xpath("//div[@id='section_left']/div[2]/a")
        self.ip_proxy = ip.text
        query = """SELECT "carmodal_proxyhma"."ip", "carmodal_proxyhma"."timest"
            FROM "carmodal_proxyhma"
            WHERE ("carmodal_proxyhma"."ip" = '%s')
            """ % self.ip_proxy
        res = []

        with get_connection().cursor() as c:
            c.execute(query, {})
            res = c.fetchall()
        if res:
            timestamp = []
            for position, item in enumerate(res):
                timestamp.append(item[1])
                if timestamp < datetime.datetime.now() - datetime.timedelta(hours=12):
                    self.change_pr()
        else:
            query = """INSERT INTO carmodal_proxyhma(ip)
                    VALUES ('%s');
                    """ % self.ip_proxy
            with get_connection() as c:
                cur = c.cursor()
                cur.execute(query, {})
                c.commit()
