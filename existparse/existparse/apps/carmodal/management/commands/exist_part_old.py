# -*- coding: utf-8 -*-
import time
from timeout import timeout
import os
import urllib
import datetime
import selenium
from django.utils.timezone import utc
from selenium.webdriver.common.proxy import *
from PIL import Image
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from django.core.management.base import BaseCommand
from carmodal.models import TradeMarkPart, ProxyList
from death_by_captcha import deathbycaptcha
# from pyvirtualdisplay import Display


def dir_exist():
    if not os.path.exists('/home/jekanchik/exist_captcha/'):
        os.mkdir('/home/jekanchik/exist_captcha/')


class Command(BaseCommand):
    recaptcha_path = "//div[@class='g-recaptcha']"
    search_path = '/html/body/div[1]/div[1]/div[2]/div[1]/form/input[2]'
    description_path = "//div[@class='description']"
    captcha_path = "//div[@class='rc-imageselect-desc-no-canonical']"
    image_captcha_path = '/home/jekanchik/exist_captcha/captcha.jpeg'
    class_title = "rc-imageselect-desc-no-canonical"
    table_path = "//div[@id='ctl00_ctl00_b_b_updPrice']/div/table/tbody/tr"
    big_table_path = "//div[@id='proxylisttable_length']//option"
    ip_path = "//table[@id='proxylisttable']//tr/td[1]"
    port_path = "//table[@id='proxylisttable']//tr/td[2]"
    driver = None

    # def __init__(self):
    #     self.driver = webdriver.Chrome(executable_path=r'/usr/bin/chromedriver')
    #     super(Command, self).__init__()

    def recaptcha(self, title):
        print title
        username = "oleg_yarosh"
        password = "rDK-AbE-7NR-wzG"
        captcha_file = '/home/jekanchik/exist_captcha/captcha.jpeg'
        banner_text = title
        client = deathbycaptcha.HttpClient(username, password)
        try:
            balance = client.get_balance()
            captcha = client.decode(
                captcha_file, type=3, banner_text=banner_text)
            if captcha:
                # print "CAPTCHA %s solved: %s" % (captcha["captcha"],
                #                                  captcha["text"])
                if '':  # check if the CAPTCHA was incorrectly solved
                    client.report(captcha["captcha"])
                else:
                    return captcha['text']
        except deathbycaptcha.AccessDeniedException:
            print "error: Access to DBC API denied," +\
                  "check your credentials and/or balance"

    def create_image_group(self, list_im, results, captcha_grid):
        time.sleep(2)
        dir_img = '/home/jekanchik/exist_captcha/'
        img = Image.open(dir_img + 'captcha.jpeg')
        for index in results:
            y = (int(index) - 1) / captcha_grid[1]
            x = ((int(index) - y * captcha_grid[1])) - 1
            image = Image.open(dir_img + '/captcha_%s.jpg' % index)
            size = image.size
            img.paste(image, (x * size[0], y * size[1]))
            img.save(dir_img + 'captcha.jpeg')
            image.close()
        for item in list_im:
            os.remove(item)

    def click_verify(self, driver):
        print 'need click verify'
        driver.find_element_by_id('recaptcha-verify-button').click()
        time.sleep(2)
        try:
            driver.switch_to_default_content()
            driver.find_element_by_id('bnSend').click()
        except:
            iframe = driver.find_elements_by_tag_name('iframe')[2]
            driver.switch_to_frame(iframe)
            self.detour_captcha_image(driver)

    def save_description(self, part_id, title, mark):
        print part_id, mark, title
        part = TradeMarkPart.objects.get(id=part_id)
        part.description = title
        part.save(update_fields=['description'])

    def else_name_tm(self, part, firmname):
        if firmname in [u'Kia', u'Hyundai']:
            firmname = u'Hyundai / Kia'
        if firmname in [u'VAG']:
            firmname = part.trade_mark.tm
        if firmname in [u'Lexus']:
            firmname = u'Toyota'
        if firmname in [u'Lexus']:
            firmname = u'Toyota'
        return firmname

    def description_in_table(self, part_id, part):
        for item in self.driver.find_elements_by_xpath(self.table_path):
            title = item.text.split('\n')[2]
            firmname = item.text.split('\n')[0]
            if part.trade_mark.tm == firmname:
                mark = firmname
                self.save_description(part_id, title, mark)
            else:
                firmname = self.else_name_tm(part, firmname)
                if part.trade_mark.tm == firmname:
                    mark = firmname
                    self.save_description(part_id, title, mark)
                else:
                    if u'(' in part.trade_mark.tm:
                        tm = part.trade_mark.tm.split('(')[0].strip(' ')
                        if tm == firmname:
                            mark = firmname
                            self.save_description(part_id, title, mark)
                        else:
                            tm = part.trade_mark.tm.split('(')[1].strip(')')
                            if tm == firmname:
                                mark = firmname
                                self.save_description(part_id, title, mark)

    def captcha_return_many_result(self, driver, results, prev_result, item):
        list_im = []
        if ',' in results:
            results = results.split(',')
        y = len(driver.find_elements_by_xpath("//tr"))
        x = len(driver.find_elements_by_xpath("//td")) / y
        captcha_grid = [x, y]
        if not prev_result:
            for result in results:
                item[int(result) - 1].click()
                time.sleep(2)
                image = driver.find_elements_by_xpath('//img'
                                                      )[int(result) - 1]\
                    .get_attribute('src')
                image_path = self.image_captcha_path.split('.')[0] + '_' +\
                    result + '.jpg'
                urllib.urlretrieve(image, image_path)
                list_im.append(image_path)
            self.create_image_group(list_im, results, captcha_grid)
            prev_result = results
        else:
            for result in results:
                item[int(prev_result[int(result) - 1]) - 1].click()
                time.sleep(2)
                image = driver.find_elements_by_xpath('//img'
                                                      )[int(result) - 1]\
                    .get_attribute('src')
                image_path = self.image_captcha_path.split('.')[0] + '_' +\
                    result + '.' + self.image_captcha_path.split('.')[1]
                urllib.urlretrieve(image, image_path)
                list_im.append(image_path)
            self.create_image_group(list_im, results, captcha_grid)
            prev_result = results

    def captcha_with_many_select(self, driver, title, image):
        res = 1
        prev_result = []
        number_iter = 0
        allow_conf = True
        while res != 0:
            time.sleep(5)
            # import ipdb; ipdb.set_trace()
            title = title.split('\n')[0]
            results = self.recaptcha(title)
            number_iter += 1
            if results:
                results = results.strip('[').strip(']')
                if len(results) < 5 and number_iter == 1:
                    allow_conf = False
                else:
                    allow_conf = True
            else:
                self.click_verify(driver)
            print results
            item = driver.find_elements_by_xpath("//tbody//td")
            if allow_conf is True:
                if u',' in results:
                    self.captcha_return_many_result(driver, results,
                                                    prev_result, item)
                elif len(results) == 1:
                    if not prev_result:
                        item[int(results) - 1].click()
                        self.captcha_return_many_result(driver, results,
                                                        prev_result, item)
                    else:
                        item[int(prev_result[int(results) - 1]) - 1].click()
                        self.captcha_return_many_result(driver, results,
                                                        prev_result, item)
                else:
                    res = 0
            else:
                self.captcha_with_many_select(driver, title, image)
        self.click_verify(driver)

    def detour_captcha_image(self, driver):
        size = len(driver.find_elements_by_xpath('//td'))
        while size != 9:
            time.sleep
            driver.find_element_by_id('recaptcha-reload-button').click()
            time.sleep(2)
            size = len(driver.find_elements_by_xpath('//td'))
        title = driver.find_element_by_class_name(
            self.class_title).text

        image = driver.find_elements_by_xpath('//img')[0]\
                      .get_attribute('src')
        dir_exist()
        urllib.urlretrieve(image, self.image_captcha_path)
        # import ipdb; ipdb.set_trace()

        if u'Click verify once there are none left.' in title:
            self.captcha_with_many_select(driver, title, image)
        else:
            results = self.recaptcha(title).strip('[')\
                                           .strip(']').split(',')
            print results
            td = driver.find_elements_by_xpath("//tbody//td")
            for result in results:
                td[int(result) - 1].click()
        self.handle(driver)

    def detour_captcha(self, driver, part):
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH,
                                            self.recaptcha_path))
        )
        if driver.find_element_by_tag_name("iframe"):
            CheckBox = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH,
                                                self.recaptcha_path
                                                )))
            CheckBox.click()
            print "[*] Clicked on checkbox"
            time.sleep(5)
            iframe = driver.find_elements_by_tag_name('iframe')[2]
            driver.switch_to_frame(iframe)
            WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH,
                                                self.captcha_path
                                                )))
            self.detour_captcha_image(driver)

    def get_free_proxy(self):
        proxy_list = []
        driver = webdriver.Firefox()
        driver.get("https://free-proxy-list.net/")
        driver.find_element_by_name("proxylisttable_length").click()
        time.sleep(2)
        driver.find_elements_by_xpath(self.big_table_path)[2].click()
        ip = driver.find_elements_by_xpath(self.ip_path)
        port = driver.find_elements_by_xpath(self.port_path)
        for position, item in enumerate(ip):
            pt = port[position].text
            proxy = item.text + ':' + pt
            proxy_list.append(proxy)
        driver.quit()
        return proxy_list

    def parse_proxy(self):
        myProxy = self.get_free_proxy()
        print myProxy
        for item in myProxy:
            chrome_option = webdriver.ChromeOptions()
            chrome_option.add_argument("--proxy-server=%s" % item)
            self.driver = webdriver.Chrome(chrome_options=chrome_option)
            self.check_proxy_server()
            try:
                if self.driver.find_element_by_name('pcode'):
                    if not ProxyList.objects.filter(proxy=item):
                        qs = ProxyList(proxy=item)
                        qs.save()
                        print 'good proxy'
                    else:
                        print 'exist'
                    self.driver.quit()
                else:
                    print 'bad proxy'
                    self.driver.quit()
            except:
                print 'bad proxy'
                self.driver.quit()

    def delete_bad_proxy(self):
        ProxyList.objects.filter(status=False).delete()
        self.parse_proxy()
        self.parse_page()

    @timeout(40)
    def check_proxy_server(self):
        self.driver.get("http://exist.ua/")

    def check_proxy(self, query):
        prev_proxy = query.proxy
        chrome_option = webdriver.ChromeOptions()
        chrome_option.add_argument("--proxy-server=%s" % query.proxy)
        self.driver = webdriver.Chrome(chrome_options=chrome_option)
        self.check_proxy_server()
        try:
            self.driver.find_element_by_name('pcode')
            print 'good proxy'
            query.start_date = datetime.datetime.utcnow().replace(tzinfo=utc)
            query.status = True
            query.save(update_fields=['status', 'start_date'])
        except:
            print 'bad proxy'
            self.driver.quit()
            query.status = False
            query.end_date = datetime.datetime.utcnow().replace(tzinfo=utc)
            query.save(update_fields=['status', 'end_date'])
            self.change_proxy(prev_proxy)
        self.parse_page(prev_proxy)

    def get_next_proxy(self, qs, query_last):
        if not qs.id == query_last.id:
            next_proxy = ProxyList.objects.filter(id__gt=qs.id).first()
            if next_proxy.end_date is not None:
                time_now = datetime.datetime.utcnow().replace(tzinfo=utc)
                div_time = time_now - next_proxy.end_date
                hour = str(div_time).split('.')[0].split(':')[0]
                if hour < 0:
                    qs = next_proxy
                    self.get_next_proxy(qs, query_last)
                else:
                    self.check_proxy(next_proxy.proxy)

            else:
                query = ProxyList.objects.get(proxy=next_proxy.proxy)
                self.check_proxy(query)
        else:
            self.delete_bad_proxy()

    def change_proxy(self, prev_proxy):
        if prev_proxy is not None:
            query_last = ProxyList.objects.last()
            qs = ProxyList.objects.filter(proxy=prev_proxy)[0]
            if not qs.id == query_last.id:
                self.get_next_proxy(qs, query_last)
            else:
                query = ProxyList.objects.first()
                tome_now = datetime.datetime.utcnow().replace(tzinfo=utc)
                div_time = tome_now - query.end_date
                print 'div time: ', (str(div_time)).split('.')[0]
                hour = str(div_time).split('.')[0].split(':')[0]
                if int(hour) > 12:
                    self.check_proxy(query)
                else:
                    self.delete_bad_proxy()
                    # div = 12 - hour
                    # print 'sleep %s hour' % div
                    # time_sleep = div * 3600
                    # time.sleep(time_sleep)
                    # query = ProxyList.objects.first()
                    # self.check_proxy(query)

        else:
            query = ProxyList.objects.filter(start_date__isnull=False,
                                             end_date__isnull=True).first()
            if query:
                self.check_proxy(query)
            else:
                query = ProxyList.objects.filter(start_date__isnull=True,
                                                 end_date__isnull=True).first()
                if query:
                    self.check_proxy(query)
                else:
                    print 'not such query'
                    query = ProxyList.objects.first()
                    self.check_proxy(query)

    def save_time(self, prev_proxy):
        qs = ProxyList.objects.filter(proxy=prev_proxy)[0]
        qs.end_date = datetime.datetime.utcnow().replace(tzinfo=utc)
        qs.save(update_fields=['end_date'])

    def h2_is_exist(self, part_id):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, "//div[@id='ajaxupdatepanel']//h2"))
            )
            is_exist = True
            inspection = TradeMarkPart.objects.get(id=part_id)
            inspection.inspection = 'not found on exist.ua'
            inspection.save(update_fields=['inspection'])
        except selenium.common.exceptions.WebDriverException:
            is_exist = False
        return is_exist

    def parse_page(self, prev_proxy=None):
        self.driver.get("http://exist.ua/")
        for part in TradeMarkPart.objects.filter(description__isnull=True,
                                                 inspection__isnull=True)[:500]:
            part_id = part.id
            try:
                search = self.driver.find_element_by_name('pcode')
            except selenium.common.exceptions.NoSuchElementException:
                self.driver.quit()
                if prev_proxy is not None:
                    self.change_proxy(prev_proxy)
                else:
                    self.change_proxy(prev_proxy=None)
            search.send_keys('')
            search.send_keys(part.part_number)
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, self.search_path))
            )
            try:
                self.driver.find_element_by_xpath(self.search_path).click()
            except selenium.common.exceptions.TimeoutException:
                self.change_proxy(prev_proxy)
            is_exist = self.h2_is_exist(part_id)
            if is_exist is False:
                try:
                    h1_path = "//div[@id='ajaxupdatepanel']//h1"
                    WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, h1_path))
                    )
                except selenium.common.exceptions.WebDriverException:
                    self.driver.quit()
                    self.save_time(prev_proxy)
                    self.change_proxy(prev_proxy)
                if not self.driver.find_elements_by_xpath(self.description_path):
                    if self.driver.find_elements_by_xpath(self.recaptcha_path):
                        self.driver.quit()
                        self.save_time(prev_proxy)
                        self.change_proxy(prev_proxy)
                        # self.detour_captcha(driver, part)
                    elif self.driver.find_elements_by_xpath(self.table_path):
                        self.description_in_table(part_id, part)
                else:
                    if self.driver.find_elements_by_xpath(self.description_path):
                        info = self.driver.find_elements_by_xpath(self.description_path
                                                                  )[1]
                        firmname = self.driver.find_elements_by_xpath(
                            "//span[@class='caname']/a")[0]
                        if info:
                            if firmname.text == part.trade_mark.tm:
                                mark = part.trade_mark.tm

                                title = info.text
                                self.save_description(part_id, title, mark)
                    else:
                        print 'not found'
                inspection = TradeMarkPart.objects.get(id=part_id)
                if inspection.description is None:
                    inspection.inspection = 'not found on exist.ua'
                    inspection.save(update_fields=['inspection'])
        self.driver.quit()

    def handle(self, *args, **options):
        self.change_proxy(prev_proxy=None)
        self.parse_page()
