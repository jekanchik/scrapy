import os
import urllib

from django.core.management.base import BaseCommand

from carmodal.models import CarModel
from existparse.settings import DOWNLOAD_MODEL_DIR

URL_TEMPLATE = "http://img.exist.ru/img.jpg?Key=%s&MethodType=5"
URL_TEMPLATE_SMALL = "http://img.exist.ru/img.jpg?" +\
                     "Key=%s&Size=150x100&MethodType=5"


def dir_exist():
    if not os.path.exists(DOWNLOAD_MODEL_DIR):
        os.mkdir(DOWNLOAD_MODEL_DIR)


class Command(BaseCommand):
    def handle(self, *args, **options):
        dir_exist()
        print 'numbers of positions'
        print CarModel.objects.count()
        print('starting parse process...')
        n = 0
        for car_model in CarModel.objects.all():
            url = URL_TEMPLATE % car_model.id
            url_small = URL_TEMPLATE_SMALL % car_model.id
            file_name = str(car_model.slug) + "_full.jpg"
            file_name_small = str(car_model.slug) + ".jpg"
            full_file_name = os.path.join(DOWNLOAD_MODEL_DIR, file_name)
            full_file_name_small = os.path.join(DOWNLOAD_MODEL_DIR,
                                                file_name_small)
            if not os.path.exists(full_file_name):
                urllib.urlretrieve(url, full_file_name)
            if not os.path.exists(full_file_name_small):
                urllib.urlretrieve(url_small, full_file_name_small)
            size_image = os.path.getsize(full_file_name)
            size_image_small = os.path.getsize(full_file_name_small)
            if size_image == 3329:
                os.remove(full_file_name)
            if size_image_small == 3329:
                os.remove(full_file_name_small)
            if os.path.isfile(full_file_name_small):
                tmp = CarModel.objects.get(id=car_model.id)
                if tmp.prod_img is None:
                    tmp.prod_img.name = full_file_name_small
                    tmp.save(update_fields=['prod_img'])
            n += 1
            print n, 'position completed', car_model.id
        print '##########################'
        print n, 'positions completed, see all in "images" folder '
