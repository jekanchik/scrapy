import time
from django.core.management.base import BaseCommand
from selenium import webdriver
from selenium.webdriver.common.proxy import *
from carmodal.models import ProxyList


class Command(BaseCommand):
    indicator_path = "//tr[@class='altshade']/td[5]/div/div"
    port = "//tr[@class='altshade']/td[3]"

    def get_free_proxy(self):
        proxy_list = []
        driver = webdriver.Firefox()
        driver.get("http://proxylist.hidemyass.com/1#listable")
        ip = driver.find_elements_by_xpath("//tr[@class='altshade']/td[2]")
        indicator = driver.find_elements_by_xpath(self.indicator_path)

        for position, item in enumerate(ip):
            ind = int(indicator[position].get_attribute("style").split(';')[0]
                                         .strip('"').split(':')[1].strip()
                                         .strip('%'))
            if ind > 50:
                port = driver.find_elements_by_xpath(self.port)[position].text
                proxy = item.text + ':' + port
                proxy_list.append(proxy)
        driver.quit()
        return proxy_list

    def handle(self, *args, **options):
        myProxy = self.get_free_proxy()
        # myProxy = "39.82.80.63:81"
        for item in myProxy:
            proxy = Proxy({
                'proxyType': ProxyType.MANUAL,
                'httpProxy': item,
                'ftpProxy': item,
                'sslProxy': item,
                'noProxy': ''})
            driver = webdriver.Firefox(proxy=proxy)
            driver.set_page_load_timeout(10)
            try:
                driver.get("http://www.google.com")
                print 'good proxy'
                if not ProxyList.objects.filter(proxy=item):
                    qs = ProxyList(proxy=item)
                    qs.save()
                driver.quit()
            except:
                print 'bad proxy'
                driver.quit()
        self.handle()
