# -*- coding: utf-8 -*-
import time
import selenium
import os
from PIL import Image
from operator import itemgetter
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from django.core.management.base import BaseCommand
from carmodal.models import TradeMarkPart


def dir_exist():
    if not os.path.exists('/home/jekanchik/exist_capcha/'):
        os.mkdir('/home/jekanchik/exist_capcha/')


class Command(BaseCommand):
    def handle(self, *args, **options):
        fp = webdriver.FirefoxProfile()
        fp.set_preference("browser.download.folderList", 2)
        fp.set_preference("browser.download.manager.showWhenStarting", False)
        fp.set_preference("browser.download.dir", os.getcwd())
        fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-msdos-program")
        driver = webdriver.Firefox(firefox_profile=fp)
        driver.get("http://exist.ua/")
        main_window = driver.current_window_handle
        for part in TradeMarkPart.objects.all()[:100]:
            search = driver.find_element_by_name('pcode')
            search.send_keys(part.part_number)
            driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[1]/form/input[2]').click()
            if not driver.find_elements_by_xpath("//div[@class='description']"):
                dir_exist()
                WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "imgCaptcha"))
                )
                captcha = driver.find_element_by_id('imgCaptcha')

                src = captcha.get_attribute('src')
                # .split('.')[0] + '.jpg'
                captcha.send_keys(Keys.CONTROL + 't')
                driver.get(src)
                path = "/home/jekanchik/exist_capcha/captcha.gif"
                ActionChains(driver).send_keys(Keys.CONTROL, "s").perform()
                # driver.save_screenshot(path)
                # driver.find_element_by_tag_name('body')\
                #     .send_keys(Keys.CONTROL + 'w')
                # im = Image.open(path)
                # im = im.convert("P")
                # his = im.histogram()
                # print his
                # values = {}
                # for i in range(1, 256):
                #     values[i] = his[i]
                # for j, k in sorted(values.items(), key=itemgetter(1), reverse=True)[:10]:
                #     print j, k
                
                # import ipdb; ipdb.set_trace()
                # time.sleep(99999999)
            else:
                info = driver.find_elements_by_xpath("//div[@class='description']")
                mark = driver.find_element_by_link_text(part.trade_mark.tm)
                print '-------------------------------------------------------'
                print part.part_number
                print mark.text
                n = 0
                for item in info:
                    if n > 0:
                        print item.text
                        n += 1
                    else:
                        n += 1
                print '-------------------------------------------------------'
        driver.quit()