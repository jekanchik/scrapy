from django.core.management.base import BaseCommand
from death_by_captcha import deathbycaptcha


class Command(BaseCommand):
    def handle(self, *args, **options):
        username = "oleg_yarosh"
        password = "rDK-AbE-7NR-wzG"
        captcha_file = "/home/jekanchik/exist_captcha/captcha.jpeg"  # image
        # banner = PATH + "banner.jpg"  # image banner
        banner_text = "Select all images with a store front."

        # client = deathbycaptcha.SocketClient(username, password)
        client = deathbycaptcha.HttpClient(username, password)
        # to use http client use: client = deathbycaptcha.HttpClient(username, password)
        try:
            balance = client.get_balance()

            # Put your CAPTCHA file name or file-like object, and optional
            # solving timeout (in seconds) here:
            captcha = client.decode(
                captcha_file, type=3, banner_text=banner_text)
            # you can supply optional `grid` argument to decode() call, with a 
            # string like 3x3 or 2x4, defining what grid individual images were located at
            # example: 
            # captcha = client.decode(
            #    captcha_file, type=3, banner=banner, banner_text=banner_text, grid="2x4")
            # see 2x4.png example image to have an idea what that images look like
            # If you wont supply `grid` argument, dbc will attempt to autodetect the grid
            if captcha:
                # The CAPTCHA was solved; captcha["captcha"] item holds its
                # numeric ID, and captcha["text"] is a json-like list of the index for each image that should be clicked.
                print "CAPTCHA %s solved: %s" % (captcha["captcha"], captcha["text"])

                if '':  # check if the CAPTCHA was incorrectly solved
                    client.report(captcha["captcha"])
        except deathbycaptcha.AccessDeniedException:
            # Access to DBC API denied, check your credentials and/or balance
            print "error: Access to DBC API denied, check your credentials and/or balance"