from django.core.management.base import BaseCommand

from carmodal.models import CarModelTrue, CarModelNeedImage


class Command(BaseCommand):
    def handle(self, *args, **options):
        for item in CarModelNeedImage.objects.all():
            qs = CarModelTrue.objects.filter(car_id=item.car_id)[0]
            item.start_date = qs.startdate_production
            item.end_date = qs.enddate_production
            print item.start_date, item.end_date
            item.save(update_fields=['start_date', 'end_date'])
