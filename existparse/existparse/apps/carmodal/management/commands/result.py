# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from carmodal.models import Buss


class Command(BaseCommand):
    def handle(self, *args, **options):
        qs_all = Buss.objects.all()
        qs_image = Buss.objects.filter(image__isnull=False)
        qs_image_false = Buss.objects.filter(image__isnull=True)
        print 'All positions:'
        print qs_all.count()
        print 'Positions with image:'
        print qs_image.count()
        print 'Positions without image:'
        print qs_image_false.count()
        print 'You can find image in folder existparse/myspider/bus/full'
