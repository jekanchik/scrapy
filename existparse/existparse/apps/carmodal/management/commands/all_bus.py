# -*- coding: utf-8 -*-
import unicodecsv as csv
from openpyxl import load_workbook
from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('all_bus.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter=';')
        workbook = load_workbook('/home/jekanchik/shiny.xlsx',
                                 use_iterators=True)
        first_sheet = workbook.get_sheet_names()[0]
        worksheet = workbook.get_sheet_by_name(first_sheet)
        n = 1
        for row in worksheet.iter_rows():
            if n > 1:
                n += 1
                if not row[9].value is None:
                    print row[5].value, '|', row[9].value
                    qs = Bus.objects.filter(part_id=row[5].value,
                                            image__isnull=False)
                    if qs:
                        writer.writerow([row[9].value, qs[0].image])
            else:
                n += 1
        to_file.close()
# import ipdb; ipdb.set_trace()
