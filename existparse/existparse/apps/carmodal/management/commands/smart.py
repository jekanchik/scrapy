import unicodecsv as csv

from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('smart_shop.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter='	')
        writer.writerow(['id',
                         'part_number_catalog',
                         'part_number_search',
                         'title', 'description',
                         'fake_number',
                         'url'])
        for item in SmartShop.objects.all():
            writer.writerow([item.id,
                             item.part_number_catalog,
                             item.part_number_search,
                             item.title,
                             item.description,
                             item.fake_number,
                             item.url])
        to_file.close()
