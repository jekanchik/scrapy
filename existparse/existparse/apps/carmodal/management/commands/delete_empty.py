import os
from django.core.management.base import BaseCommand

from carmodal.models import CarModelNeedImage


class Command(BaseCommand):
    def handle(self, *args, **options):
        images = []
        for item in CarModelNeedImage.objects.filter(image__isnull=False):
            image = item.image.name
            name = image.replace('full/', '')
            images.append(name)
        directory = '/home/jekanchik/cars/full'
        files = os.listdir(directory)
        for name in files:
            if name not in images:
                os.remove('/home/jekanchik/cars/full/' + name)
            else:
                print 'stay'
