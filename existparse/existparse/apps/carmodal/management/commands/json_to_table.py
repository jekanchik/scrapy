import json

from django.core.management.base import BaseCommand

from carmodal.models import CarModelTrue


class Command(BaseCommand):
    def handle(self, *args, **options):
        file_path = '/home/jekanchik/car_model_with_pic.json'
        with open(file_path) as data_file:
            data = json.load(data_file)
            for item in data:
                CarModelTrue.objects.create(car_id=item['pk'],
                                            **item['fields'])
