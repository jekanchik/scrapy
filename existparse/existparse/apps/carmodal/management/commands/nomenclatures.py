# -*- coding: utf-8 -*-
import re
import unicodecsv as csv
from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        file = open("nomenclatures.csv", 'r')
        to_file = open('good_nomencl.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter=';')
        n = 0
        for line in file:
            if n > 0:
                # if n < 10:
                line = line.decode('cp1251').encode('utf-8')
                raw = line.split(';')
                part_id = raw[6].strip('\n\r\t')
                if not int(part_id):
                    part_id = raw[7].strip('\n\r\t')
                    title = raw[6]
                    key = raw[1]
                    brand = raw[5]
                else:
                    part_id = raw[6].strip('\n\r\t')
                    title = raw[5]
                    key = raw[1]
                    brand = raw[4]
                print title
                title = title.strip('"').strip(' ').strip('"').split('""')[-1]
                title = re.search(r'.+(?=\d.*R\d)|.+(?=\d.* \wR\d)|' +
                                  '.+(?=\d.+\/\d+R.\d)|.+\d.+х\d',
                                  title).group()
                title = title.split(' ')
                del title[-1]
                title = ' '.join(title)
                if '(' in title:
                    delete = re.search(r'\(.+\)', title).group()
                    title = title.replace(delete, '')
                writer.writerow([key,
                                 part_id,
                                 brand,
                                 title])
                n += 1
            else:
                n += 1
