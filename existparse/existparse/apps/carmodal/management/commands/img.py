import unicodecsv as csv

from django.core.management.base import BaseCommand

from carmodal.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_file = open('rozetka_image2.csv', 'w+')
        writer = csv.writer(to_file, encoding='cp1251', delimiter=';')
        for car_bus in RozetkaFiltered.objects.all()[665:]:
            writer.writerow([car_bus.id_brand, car_bus.image])
        to_file.close()
