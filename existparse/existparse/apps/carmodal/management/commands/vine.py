import time
import datetime
import unicodecsv as csv
from selenium import webdriver
from existparse.settings import PATH_TO_CHROME
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def __init__(self):
        self.driver = webdriver.Chrome(executable_path=PATH_TO_CHROME)
        super(Command, self).__init__()

    def handle(self, *args, **options):
        print datetime.datetime.now()
        to_file = open('url.csv', 'w+')
        writer = csv.writer(to_file, encoding='cp1251')
        self.driver.get('https://vine.co/playlists/')
        time.sleep(2.5)
        playlists = []
        n = 0
        code = None
        for n in range(1, 1000):
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(3)
            if code != self.driver.page_source:
                code = self.driver.page_source
            else:
                break
            n += 1
        for item in self.driver.find_elements_by_class_name("ember-view"):
            if item.get_attribute("href") and u'playlists' in item.get_attribute("href"):
                playlists.append(item.get_attribute("href"))
        video_url = []
        for item in playlists:
            self.driver.get(item)
            time.sleep(3)
            n = 0
            code = None
            for n in range(1, 1000):
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(3)
                if code != self.driver.page_source:
                    code = self.driver.page_source
                else:
                    break
                n += 1
            for item in self.driver.find_elements_by_class_name("ember-view"):
                if item.get_attribute("href") and u'/v/' in item.get_attribute("href"):
                    video_url.append(item.get_attribute("href"))
        for url in video_url:
            writer.writerow([url])
        to_file.close()
        self.driver.close()
        print datetime.datetime.now()
