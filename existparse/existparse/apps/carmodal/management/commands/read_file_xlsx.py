# -*- coding: utf-8 -*-
import os
import unicodecsv as csv
from openpyxl import load_workbook
from django.core.management.base import BaseCommand
from carmodal.models import Bus


class Command(BaseCommand):
    def handle(self, *args, **options):
        list_dir = os.listdir('/home/jekanchik/scrapy_education/existparse/myspider/buss/full/')
        images = map(lambda x: 'full/' + x, list_dir)
        file = open("/home/jekanchik/shini_not_img.csv", 'r')
        list_id = []
        n = 1
        for line in file:
            if n > 1:
                n += 1
                list_id.append(line.strip('\n\t'))
            else:
                n += 1
        file.close()
        workbook = load_workbook('/home/jekanchik/shiny.xlsx',
                                 use_iterators=True)
        first_sheet = workbook.get_sheet_names()[0]
        worksheet = workbook.get_sheet_by_name(first_sheet)
        n = 1
        to_file = open('bus_image_div.csv', 'w+')
        writer = csv.writer(to_file, encoding='utf-8', delimiter=';')
        for row in worksheet.iter_rows():
            if n > 1:
                n += 1
                if str(row[9].value) in list_id:
                    qs = Bus.objects.filter(part_id=row[0].value,
                                            image__isnull=False)
                    if len(qs) != 0:
                        query = qs.first()
                        if query.image.name in images:
                            print row[9].value, query.image.name
                            writer.writerow([row[9].value, query.image.name])

            else:
                n += 1
