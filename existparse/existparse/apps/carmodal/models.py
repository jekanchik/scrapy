# coding: utf-8
from __future__ import unicode_literals

from django.db import models


class ProxyHMA(models.Model):
    ip = models.CharField(u"Ip", max_length=255)
    timest = models.DateTimeField(verbose_name=u"Time closing proxy",
                                  blank=True,
                                  null=True)


class TradeMark(models.Model):
    car_id = models.IntegerField(verbose_name=u"Ид", primary_key=True)
    tm = models.CharField(u"Торговая марка", max_length=255)
    tm_two = models.CharField(u"Торговая марка", max_length=255)
    tm_three = models.CharField(u"Торговая марка", max_length=255)
    tm_bool = models.BooleanField(u"Что-то очень важное", default=False)
    car_else_id = models.CharField(u"Что-то очень важное", max_length=255,
                                   null=True, blank=True)
    somthing = models.CharField(u"Что-то очень важное", max_length=255,
                                null=True, blank=True)
    somthing_else = models.CharField(u"Что-то очень важное",
                                     max_length=255,
                                     null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.tm


class TradeMarkPart(models.Model):
    id_automobi = models.IntegerField(verbose_name=u"Ид", null=True)
    part_number = models.CharField(u"Код поиска", max_length=255)
    part_number_two = models.CharField(u"Код поиска", max_length=255)
    info = models.CharField(u"Описание", max_length=255)
    trade_mark = models.ForeignKey('TradeMark', verbose_name=u"Торговая марка")
    description = models.CharField(u"Описание", max_length=255, null=True)
    inspection = models.CharField(u"Проверка", max_length=255, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.part_number, self.trade_mark)


class FirstForm(models.Model):
    subject = models.CharField(u"Тема сообщения", max_length=255)
    message = models.CharField(u"Сообщения", max_length=255)
    sender = models.EmailField(u"Email", max_length=254)
    cc_myself = models.BooleanField(u"ХЗ", default=False)
    mugshot = models.ImageField(u"Изображение",
                                upload_to='/home/jekanchik/scrapy_education/existparse/existparse/media')


class CarModelNeedImage(models.Model):
    car_id = models.IntegerField(verbose_name=u"Ид", null=True)
    car_brand = models.CharField(u"Бренд Авто", max_length=255)
    car_model = models.CharField(u"Модель Авто", max_length=255)
    body_code = models.CharField(u"Код кузова", max_length=255, null=True)
    error = models.CharField(u"Ошибка", max_length=255, null=True)
    start_date = models.DateField(verbose_name=u"Дата " +
                                  "Начала Производства",
                                  blank=True,
                                  null=True)
    end_date = models.DateField(verbose_name=u"Дата " +
                                "Окончания Производства",
                                blank=True,
                                null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 null=True)
    comments = models.CharField(u"Коментарии", max_length=255, null=True)
    image = image = models.ImageField(u"Изображение",
                                      upload_to='/cars', null=True)
    name_true = models.CharField(u"картинка", max_length=255, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.car_model, self.car_brand)

    class Meta:
        verbose_name = u"модель автомобиля"
        verbose_name_plural = u"Модели автомобилей"


class CarModelTrue(models.Model):
    car_id = models.IntegerField(verbose_name=u"Ид", null=True)
    car_brand = models.IntegerField(verbose_name=u"Марка автомобиля")
    car_type_shot_key = models.CharField(u"Тип Авто", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    tcartype = models.CharField(u"Описание",
                                max_length=255,
                                blank=True,
                                null=True)
    startdate_production = models.DateField(verbose_name=u"Дата " +
                                            "Начала Производства",
                                            blank=True,
                                            null=True)
    enddate_production = models.DateField(verbose_name=u"Дата " +
                                          "Окончания Производства",
                                          blank=True,
                                          null=True)
    is_passenger = models.BooleanField(u"Пассажирский", default=False)
    is_commercial = models.BooleanField(u"Коммерческий", default=False)
    is_axis_manufacturer = models.BooleanField(u"Ось", default=False)
    img = models.ImageField(u"Изображение",
                            upload_to='/existparse/image/',
                            blank=True, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.car_type_shot_key, self.car_brand.title)

    class Meta:
        verbose_name = u"модель автомобиля"
        verbose_name_plural = u"Модели автомобилей"


class CarModel(models.Model):
    car_brand = models.ForeignKey('CarBrand', verbose_name=u"Марка автомобиля")
    body_code = models.CharField(u"Тип кузова", max_length=255,
                                 blank=True, null=True)
    car_type_shot_key = models.CharField(u"Тип Авто", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    tcartype = models.CharField(u"Описание",
                                max_length=255,
                                blank=True,
                                null=True)
    startdate_production = models.DateField(verbose_name=u"Дата " +
                                            "Начала Производства",
                                            blank=True,
                                            null=True)
    enddate_production = models.DateField(verbose_name=u"Дата " +
                                          "Окончания Производства",
                                          blank=True,
                                          null=True)
    is_passenger = models.BooleanField(u"Пассажирский", default=False)
    is_commercial = models.BooleanField(u"Коммерческий", default=False)
    is_axis_manufacturer = models.BooleanField(u"Ось", default=False)
    exist_modification = models.CharField(u"Модификация",
                                          max_length=255,
                                          blank=True,
                                          null=True)
    exist_years = models.CharField(u"Дата начала-окончания" +
                                   "производства",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    exist_engine_type = models.CharField(u"Тип двигателя",
                                         max_length=255,
                                         blank=True,
                                         null=True)
    exist_engine_model = models.CharField(u"Модель двигателя",
                                          max_length=255,
                                          blank=True,
                                          null=True)
    exist_transmission = models.CharField(u"Тип КПП",
                                          max_length=255,
                                          blank=True,
                                          null=True)
    exist_doors = models.CharField(u"Количество дверей",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    exist_privod = models.CharField(u"Привод",
                                    max_length=255,
                                    blank=True,
                                    null=True)
    prod_img = models.ImageField(u"Изображение",
                                 upload_to='/existparse/image/',
                                 blank=True, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.car_type_shot_key, self.car_brand.title)

    class Meta:
        verbose_name = u"модель автомобиля"
        verbose_name_plural = u"Модели автомобилей"


class CarModification(models.Model):
    car_model = models.ForeignKey(CarModel, verbose_name=u"Модель автомобиля")
    show_name = models.CharField(u"Описание Модификации", max_length=255)
    full_name = models.CharField(u"Полное Описание", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    engine_modifications = models.CharField(verbose_name=u"Модификация мотора",
                                            max_length=255,
                                            blank=True,
                                            null=True)
    startdate_production = models.DateField(verbose_name=u"Дата" +
                                            "Начала Производства",
                                            blank=True,
                                            null=True)
    enddate_production = models.DateField(verbose_name=u"Дата" +
                                          "Окончания Производства",
                                          blank=True,
                                          null=True)
    kw_engine_power = models.PositiveIntegerField(u"Мощность Двигателя (kW)",
                                                  blank=True, null=True)
    hp_engine_power = models.PositiveIntegerField(u"Мощность Двигателя в (HP)",
                                                  blank=True, null=True)
    engine_volume = models.PositiveIntegerField(u"Объем Двигателя (см3)",
                                                blank=True, null=True)
    cylinders_num = models.PositiveIntegerField(u"Кол-во Цилиндров",
                                                blank=True, null=True)
    doors_num = models.PositiveIntegerField(u"Кол-во Дверей",
                                            blank=True, null=True)
    tank_volume = models.PositiveIntegerField(u"Объем Бака (л)",
                                              blank=True, null=True)
    valves_per_cylinder = models.PositiveIntegerField(u"Клапанов на Цилиндр",
                                                      blank=True, null=True)
    voltage = models.IntegerField(verbose_name=u"Код Вольтажа",
                                  blank=True, null=True)
    abs = models.IntegerField(verbose_name=u"Тип ABS",
                              blank=True, null=True)
    asr = models.IntegerField(verbose_name=u"Тип ASR",
                              blank=True, null=True)
    engine = models.IntegerField(verbose_name=u"Тип Двигателя",
                                 blank=True, null=True)
    braketype = models.IntegerField(verbose_name=u"Тип Тормозной Системы",
                                    blank=True, null=True)
    brakesys = models.IntegerField(verbose_name=u"Код Тормозной Системы",
                                   blank=True, null=True)
    fuel = models.IntegerField(verbose_name=u"Код Топлива",
                               blank=True, null=True)
    catalyst = models.IntegerField(verbose_name=u"Тип Катализатора",
                                   blank=True, null=True)
    body = models.IntegerField(verbose_name=u"Тип Кузова",
                               blank=True, null=True)
    chassis = models.IntegerField(verbose_name=u"Тип Шасси",
                                  blank=True, null=True)
    axle = models.IntegerField(verbose_name=u"Тип Оси",
                               blank=True, null=True)
    drive = models.IntegerField(verbose_name=u"Код Привода",
                                blank=True, null=True)
    transmission = models.IntegerField(verbose_name=u"Код Трансмиссии",
                                       blank=True, null=True)
    fuelin = models.IntegerField(verbose_name=u"Код Системы Впрыска",
                                 blank=True, null=True)
    mod_img = models.ImageField(u"Изображение",
                                upload_to='/existparse/car_images/',
                                blank=True, null=True)
    extra_data = models.TextField(verbose_name=u"Описание",
                                  blank=True,
                                  null=True)

    def __unicode__(self):
        return u'%s | %s | %s | %s | %s' % (self.show_name, self.engine_volume,
                                            self.hp_engine_power, self.engine,
                                            self.body)


class CarTreeProperty(models.Model):
    title = models.TextField(verbose_name=u"Запчасти",
                             blank=True,
                             null=True)
    img = models.ImageField(u"Изображение",
                            upload_to='/myspider/ukr_images/full/',
                            blank=True, null=True)
    car_modification = models.ForeignKey(CarModification,
                                         verbose_name=u"Модель автомобиля",
                                         blank=True,
                                         null=True)


class AutomobyPartShiny(models.Model):
    mark_key = models.CharField(verbose_name=u"Код товара",
                                max_length=255,
                                blank=True,
                                null=True)
    mark = models.CharField(verbose_name=u"Производитель",
                            max_length=255,
                            blank=True,
                            null=True)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255,
                             blank=True,
                             null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 db_index=True,
                                 blank=True,
                                 null=True)


class MyCartShiny(models.Model):
    mark = models.CharField(verbose_name=u"Производитель",
                            max_length=255,
                            blank=True,
                            null=True)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255,
                             blank=True,
                             null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/shinu_images/full/',
                              blank=True, null=True)


class CarShinyImage(models.Model):
    id_carbrand = models.IntegerField(verbose_name=u"Код",
                                      blank=True, null=True)
    mark_key = models.CharField(verbose_name=u"Код товара",
                                max_length=255,
                                blank=True,
                                null=True)
    mark = models.CharField(verbose_name=u"Производитель",
                            max_length=255,
                            blank=True,
                            null=True)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255,
                             blank=True,
                             null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/shinu_images/full/',
                              blank=True, null=True)


class ShypShyna(models.Model):
    key = models.CharField(verbose_name=u"Код",
                           max_length=255,
                           blank=True,
                           null=True)
    title = models.CharField(verbose_name=u"Бренд Модель",
                             max_length=255)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255,
                             blank=True,
                             null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/shyp_shyna/full/',
                              blank=True, null=True)


class RozetkaInfo(models.Model):
    brand = models.CharField(verbose_name=u"Производитель",
                             max_length=255)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255)
    image_url = models.CharField(verbose_name=u"Изображение",
                                 max_length=255,
                                 blank=True,
                                 null=True)
    image_url_full = models.CharField(verbose_name=u"Большое изображение",
                                      max_length=255,
                                      blank=True,
                                      null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 db_index=True,
                                 blank=True,
                                 null=True)


class RozetkaFiltered(models.Model):
    brand = models.CharField(verbose_name=u"Производитель",
                             max_length=255)
    model = models.CharField(verbose_name=u"Модель",
                             max_length=255)
    image_url = models.CharField(verbose_name=u"Изображение",
                                 max_length=255,
                                 blank=True,
                                 null=True)
    image_url_full = models.CharField(verbose_name=u"Большое изображение",
                                      max_length=255,
                                      blank=True,
                                      null=True)
    id_brand = models.IntegerField(verbose_name=u"Код",
                                   blank=True, null=True)
    key_brand = models.IntegerField(verbose_name=u"Код",
                                    blank=True, null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/rozetka/full/',
                              blank=True, null=True)


class SmartShop(models.Model):
    title = models.TextField(verbose_name=u"Заголовок",
                             blank=True,
                             null=True)
    description = models.TextField(verbose_name=u"Описание",
                                   blank=True,
                                   null=True)
    part_number_catalog = models.CharField(verbose_name=u"Каталожный номер",
                                           max_length=255,
                                           blank=True,
                                           null=True)
    part_number_search = models.CharField(verbose_name=u"Поисковый номер",
                                          max_length=255,
                                          blank=True,
                                          null=True)
    fake_number = models.CharField(verbose_name=u"Подозрительный",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    url = models.CharField(verbose_name=u"Ссылка",
                           max_length=255,
                           blank=True,
                           null=True)


class AllBus(models.Model):
    title = models.CharField(verbose_name=u"Модель",
                             max_length=255,
                             blank=True,
                             null=True)
    key = models.CharField(verbose_name=u"Код",
                           max_length=255,
                           blank=True,
                           null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/bus_images/full/',
                              blank=True, null=True)
    parse_from = models.CharField(verbose_name=u"Код",
                                  max_length=255,
                                  blank=True,
                                  null=True)
    length = models.IntegerField(verbose_name=u"Длина строки",
                                 blank=True, null=True)


class FileBus(models.Model):
    brand_model = models.CharField(verbose_name=u"Модель",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    key = models.CharField(verbose_name=u"Код с шип шина",
                           max_length=255,
                           blank=True,
                           null=True)
    id_key = models.IntegerField(verbose_name=u"Код товара",
                                 blank=True,
                                 null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 db_index=True,
                                 blank=True,
                                 null=True)
    image_url = models.CharField(verbose_name=u"Ссылка на маленькую картинку",
                                 max_length=255,
                                 blank=True,
                                 null=True)
    image_url_full = models.CharField(verbose_name=u"Ссылка на большую картинку",
                                      max_length=255,
                                      blank=True,
                                      null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/bus_images/full/',
                              blank=True, null=True)


class Bus(models.Model):
    part_id = models.BigIntegerField(verbose_name=u"Код товара",
                                     blank=True,
                                     null=True)
    brand_model = models.CharField(verbose_name=u"Строка для сровнения",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 db_index=True,
                                 blank=True,
                                 null=True)
    image_url = models.CharField(verbose_name=u"Ссылка на маленькую картинку",
                                 max_length=255,
                                 blank=True,
                                 null=True)
    image_url_full = models.CharField(verbose_name=u"Ссылка на большую картинку",
                                      max_length=255,
                                      blank=True,
                                      null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/bus/full/',
                              blank=True, null=True)


class Buss(models.Model):
    part_id = models.BigIntegerField(verbose_name=u"Код товара",
                                     blank=True,
                                     null=True)
    new_id = models.IntegerField(verbose_name=u"Код",
                                 blank=True,
                                 null=True)
    brand_model = models.CharField(verbose_name=u"Строка для сровнения",
                                   max_length=255,
                                   blank=True,
                                   null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 db_index=True,
                                 blank=True,
                                 null=True)
    image_url = models.CharField(verbose_name=u"Ссылка на маленькую картинку",
                                 max_length=255,
                                 blank=True,
                                 null=True)
    image_url_full = models.CharField(verbose_name=u"Ссылка на большую картинку",
                                      max_length=255,
                                      blank=True,
                                      null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/myspider/bus/full/',
                              blank=True, null=True)


class CarBrand(models.Model):
    title = models.CharField(u"Название", max_length=255)
    title_rus = models.CharField(u"Название на русском", max_length=255,
                                 blank=True, null=True)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    start_date = models.DateField(verbose_name=u"Год начала бренда",
                                  blank=True, null=True)
    end_date = models.DateField(verbose_name=u"Год конца бренда",
                                blank=True, null=True)
    is_passenger = models.BooleanField(u"Пассажирский", default=False)
    is_commercial = models.BooleanField(u"Коммерческий", default=False)
    is_axis_manufacturer = models.BooleanField(u"Ось", default=False)
    logo = models.ImageField(
        verbose_name=u"Иконка",
        upload_to="car_brand_logos",
        blank=True, null=True
    )
    is_shown = models.BooleanField(u"Показывать", default=True)
    to_main = models.BooleanField(u"Показывать на главной", default=True)


class ExistCarBrand(models.Model):
    car_brand = models.CharField(u"Производитель", max_length=255)
    car_model = models.CharField(u"Модель", max_length=255)
    car_type = models.CharField(u"Тип", max_length=255)
    body_code = models.CharField(u"Код кузова", max_length=255,
                                 blank=True,
                                 null=True)
    car_id = models.IntegerField(verbose_name=u"Код",
                                 null=True)
    start_date = models.IntegerField(verbose_name=u"Год начала бренда",
                                     blank=True, null=True)
    end_date = models.IntegerField(verbose_name=u"Год конца бренда",
                                   blank=True, null=True)
    image = models.ImageField(u"Изображение",
                              upload_to='/cars', null=True)
    length = models.IntegerField(verbose_name=u"Количество символов",
                                 null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.car_model, self.car_brand)


class ProxyList(models.Model):
    proxy = models.CharField(u"Прокси сервер", max_length=255)
    start_date = models.DateTimeField(verbose_name=u"Дата запуска",
                                      blank=True, null=True)
    end_date = models.DateTimeField(verbose_name=u"Дата окончания",
                                    blank=True, null=True)
    status = models.BooleanField(u"Работоспособность", default=False)


class ProxyListTwo(models.Model):
    sity = models.CharField(u"Прокси сервер", max_length=255, null=True)
    ip_address = models.CharField(u"Прокси сервер", max_length=255, null=True)
    status = models.BooleanField(u"Работоспособность", default=True)
