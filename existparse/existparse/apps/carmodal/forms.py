from django import forms
from django.forms import ModelForm, Textarea, FileInput
from carmodal.models import FirstForm


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100, initial='Your name')


class ContactForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required'

    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)


class ContactFormWithMugshot(ModelForm):
    class Meta:
        model = FirstForm
        fields = '__all__'
        widgets = {
            'message': Textarea(attrs={'cols': 31, 'rows': 2})
        }

    class Media:
        css = {
            'all': ('css/firstform.css',)
        }


class CommentForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'special'}))
    url = forms.URLField()
    comment = forms.CharField(widget=forms.TextInput(attrs={'size': '40'}))
