from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .forms import NameForm
from .forms import ContactFormWithMugshot

from carmodal.models import FirstForm


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'name.html', {'form': form})


def get_contact(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        a = FirstForm.objects.filter(subject=request.POST['subject'], message=request.POST['message']).first()
        form = ContactFormWithMugshot(request.POST, request.FILES, instance=a)
        # check whether it's valid:
        if form.is_valid() and len(request.FILES) > 0:
            form.save()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('get_q'))
        else:
            print form.errors

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactFormWithMugshot(instance=FirstForm.objects.get(pk=1))

    return render(request, 'contact.html', {'form': form})


def get_q(request):
    return render(request, 'thanks.html')
